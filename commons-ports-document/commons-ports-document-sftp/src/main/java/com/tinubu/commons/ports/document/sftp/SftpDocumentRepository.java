/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.file.remote.session.Session;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.stereotype.Repository;

import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.tinubu.commons.ddd.domain.specification.Specification;
import com.tinubu.commons.lang.datetime.converter.DateTimeConverter;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentId;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.FilteringDocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.StreamDocumentContent;
import com.tinubu.commons.ports.document.domain.StreamDocumentContent.StreamDocumentContentBuilder;

@Repository("commons-ports.document.sftp.SftpDocumentRepository")
public class SftpDocumentRepository implements DocumentRepository {
   private static final Logger log = LoggerFactory.getLogger(SftpDocumentRepository.class);

   private static final StopWatch watch = new StopWatch();

   private final SessionFactory<LsEntry> sftpSessionFactory;
   private final Charset documentEncoding;
   private final DateTimeConverter dateTimeConverter;

   @Autowired
   public SftpDocumentRepository(SftpDocumentConfigPoint sftpDocumentConfig,
                                 SessionFactory<LsEntry> sftpSessionFactory,
                                 DateTimeConverter dateTimeConverter) {
      this.sftpSessionFactory = sftpSessionFactory;
      this.documentEncoding = sftpDocumentConfig.getDocumentEncoding();
      this.dateTimeConverter = dateTimeConverter;
   }

   @Override
   public Optional<Document> findDocumentById(DocumentId documentId) {
      try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
         Path sftpFile = sftpFile(documentId);

         return listDocumentEntry(session, sftpFile).map(entry -> new DocumentBuilder()
               .reconstitute()
               .documentEntry(entry)
               .content(sftpDocumentContent(session, sftpFile))
               .build());
      }
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Specification<DocumentEntry> specification) {
      try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
         return findDocumentEntriesBySpecification(specification).map(entry -> new DocumentBuilder()
               .reconstitute()
               .documentEntry(entry)
               .content(sftpDocumentContent(session, sftpFile(entry.documentId())))
               .build());
      }
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentId documentId) {
      try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
         Path sftpFile = sftpFile(documentId);

         return listDocumentEntry(session, sftpFile);
      }
   }

   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Specification<DocumentEntry> specification) {
      try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
         Stream<DocumentEntry> documentEntries;

         if (specification instanceof FilteringDocumentEntrySpecification) {
            FilteringDocumentEntrySpecification documentEntrySpecification =
                  (FilteringDocumentEntrySpecification) specification;

            if (documentEntrySpecification.directories().isEmpty()) {
               documentEntries = listAllDocumentEntries(session);
            } else {
               documentEntries = documentEntrySpecification
                     .directories()
                     .stream()
                     .flatMap(directory -> listDocumentEntries(session, directory, false));
            }
         } else {
            documentEntries = listAllDocumentEntries(session);
         }

         return documentEntries.filter(specification::satisfiedBy);
      }
   }

   @Override
   public boolean saveDocument(Document document, boolean override) {
      watch.reset();
      watch.start();
      try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
         Path sftpFile = sftpFile(document.documentId());

         if (!override && session.exists(sftpFile.toString())) {
            return false;
         }

         if (sftpFile.getParent() != null) {
            sftpMkdirs(session, sftpFile.getParent());
         }

         try (InputStream inputStream = document.content().inputStreamContent()) {
            session.write(inputStream, sftpFile.toString());
         }
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
      watch.stop();

      log.info("Document '{}' successfully saved in {}ms",
               document.documentId().idString(),
               watch.getTime(TimeUnit.MILLISECONDS));

      return true;
   }

   @Override
   public boolean deleteDocument(DocumentId documentId) {
      watch.reset();
      watch.start();
      try (Session<LsEntry> session = sftpSessionFactory.getSession()) {
         String sftpFile = sftpFile(documentId).toString();

         if (!session.exists(sftpFile)) {
            return false;
         }
         if (!session.remove(sftpFile)) {
            return false;
         }

      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
      watch.stop();

      log.info("Document '{}' successfully deleted in {}ms",
               documentId.idString(),
               watch.getTime(TimeUnit.MILLISECONDS));

      return true;
   }

   /**
    * List specified document entry.
    *
    * @param session SFTP session
    * @param sftpFile SFTP file
    */
   private Optional<DocumentEntry> listDocumentEntry(Session<LsEntry> session, Path sftpFile) {
      notNull(session, "session");
      notNull(sftpFile, "sftpFile");

      Path sftpFileParent = nullable(sftpFile.getParent(), Path.of(""));

      return listDocumentEntries(session, sftpFileParent, false)
            .filter(entry -> entry.documentId().value().getFileName().equals(sftpFile.getFileName()))
            .findAny();
   }

   /**
    * List recursively all document entries.
    *
    * @param session SFTP session
    */
   private Stream<DocumentEntry> listAllDocumentEntries(Session<LsEntry> session) {
      return listDocumentEntries(session, Path.of(""), true);
   }

   /**
    * List document entries from specified base directory. Base directory represents a physical path on the
    * SFTP. It can be relative or absolute, empty or not.
    *
    * @param session SFTP session
    * @param baseDirectory base directory to search from
    * @param recursive search recursively into sub-directories
    */
   private Stream<DocumentEntry> listDocumentEntries(Session<LsEntry> session,
                                                     Path baseDirectory,
                                                     boolean recursive) {
      notNull(session, "session");
      notNull(baseDirectory, "baseDirectory");

      String sessionBaseDirectory = baseDirectory.toString().isEmpty() ? "/" : baseDirectory.toString();

      try {
         if (session.exists(sessionBaseDirectory)) {
            return Stream
                  .of(session.list(sessionBaseDirectory))
                  .filter(traversalEntry().negate())
                  .flatMap(lsEntry -> {
                     Path sftpFile = baseDirectory.resolve(lsEntry.getFilename());
                     if (recursive && lsEntry.getAttrs().isDir()) {
                        return listDocumentEntries(session, sftpFile, recursive);
                     } else if (lsEntry.getAttrs().isReg()) {
                        return Stream.of(lsEntry).map(entry -> sftpDocumentEntry(sftpFile, entry));
                     } else {
                        return Stream.empty();
                     }
                  });
         } else {
            return Stream.empty();
         }
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   private Predicate<LsEntry> traversalEntry() {
      return lsEntry -> {
         String filename = lsEntry.getFilename();

         return filename.equals(".") || filename.equals("..");
      };
   }

   private DocumentEntry sftpDocumentEntry(Path sftpFile, LsEntry entry) {
      return new DocumentEntryBuilder()
            .reconstitute()
            .documentId(documentId(sftpFile))
            .metadata(sftpDocumentMetadata(sftpFile, entry))
            .build();
   }

   private DocumentMetadata sftpDocumentMetadata(Path sftpFile, LsEntry entry) {
      return new DocumentMetadataBuilder()
            .reconstitute()
            .documentName(documentId(sftpFile).value())
            .contentSize(entry.getAttrs().getSize())
            .contentType(documentEncoding)
            .creationDate(null)
            .lastUpdateDate(Instant.ofEpochSecond(entry.getAttrs().getMTime()))
            .build();
   }

   private StreamDocumentContent sftpDocumentContent(Session<LsEntry> session, Path sftpFile) {
      try {
         return new StreamDocumentContentBuilder()
               .content(session.readRaw(sftpFile.toString()), documentEncoding)
               .build();
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Generate real SFTP path for specified document.
    *
    * @param documentId document id
    *
    * @return real document SFTP path
    */
   private Path sftpFile(DocumentId documentId) {
      return documentId.value();
   }

   /**
    * Returns document identifier from SFTP document file. Reverses real path to generate a
    * logical document path.
    *
    * @param sftpFile SFTP document file
    *
    * @return document identifier from SFTP file
    */
   private DocumentId documentId(Path sftpFile) {
      return new DocumentId(sftpFile);
   }

   /**
    * Create specified directories recursively. Path represents a directory.
    *
    * @param session SFTP session
    * @param directory directory to create
    *
    * @throws IOException if an I/O error occurs
    */
   private void sftpMkdirs(Session<LsEntry> session, Path directory) throws IOException {
      notNull(session, "session");
      notNull(directory, "directory");

      _sftpMkdirs(session, directory);
   }

   private void _sftpMkdirs(Session<LsEntry> session, Path path) throws IOException {
      if (path != null) {
         _sftpMkdirs(session, path.getParent());
         if (!session.exists(path.toString())) {
            session.mkdir(path.toString());
         }
      }
   }

}
