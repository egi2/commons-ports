/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.sftp;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.StringJoiner;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "tinubu.commons-ports.document.sftp", ignoreUnknownFields = false)
public class SftpDocumentConfigPoint {

   /**
    * SFTP server host.
    */
   @NotBlank
   private String host;

   /**
    * SFTP server port.
    */
   @NotNull
   private Integer port;

   /**
    * SFTP server authentication username.
    */
   @NotBlank
   private String username;

   /**
    * SFTP server authentication password.
    */
   @NotBlank
   private String password;

   /**
    * Default encoding for documents.
    */
   @NotNull
   private Charset documentEncoding = StandardCharsets.UTF_8;

   public String getHost() {
      return host;
   }

   public void setHost(String host) {
      this.host = host;
   }

   public Integer getPort() {
      return port;
   }

   public void setPort(Integer port) {
      this.port = port;
   }

   public String getUsername() {
      return username;
   }

   public void setUsername(String username) {
      this.username = username;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public Charset getDocumentEncoding() {
      return documentEncoding;
   }

   public void setDocumentEncoding(Charset documentEncoding) {
      this.documentEncoding = documentEncoding;
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", SftpDocumentConfigPoint.class.getSimpleName() + "[", "]")
            .add("host='" + host + "'")
            .add("port=" + port)
            .add("username='" + username + "'")
            .add("password='" + password + "'")
            .add("documentEncoding=" + documentEncoding)
            .toString();
   }
}
