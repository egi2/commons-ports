/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.sshd.common.file.virtualfs.VirtualFileSystemFactory;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.sftp.server.SftpSubsystemFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;

import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.tinubu.commons.lang.datetime.converter.UtcDateTimeConverter;
import com.tinubu.commons.ports.document.domain.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.sftp.SftpDocumentConfigPoint;
import com.tinubu.commons.ports.document.sftp.SftpDocumentRepository;

class SftpDocumentRepositoryTest extends CommonDocumentRepositoryTest {

   /** SFTP server storage path. */
   private static Path TEST_STORAGE_PATH;

   private static SshServer server;
   private static SessionFactory<LsEntry> sftpSession;
   private SftpDocumentRepository documentRepository;

   @BeforeAll
   public static void startSftpServer() throws IOException {
      try {
         TEST_STORAGE_PATH = Files.createTempDirectory("chassis-document-sftp-test");

         System.out.printf("Creating %s as SFTP storage directory%n", TEST_STORAGE_PATH);

      } catch (IOException e) {
         throw new IllegalStateException(e);
      }

      server = SshServer.setUpDefaultServer();
      server.setPasswordAuthenticator((arg0, arg1, arg2) -> true);
      server.setPort(0);

      Path hostKey = Files.createTempFile("hostkey", ".ser");
      hostKey.toFile().deleteOnExit();

      server.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(hostKey));
      server.setSubsystemFactories(List.of(new SftpSubsystemFactory()));
      server.setFileSystemFactory(new VirtualFileSystemFactory(TEST_STORAGE_PATH));

      server.start();

      sftpSession = sftpSession(server);
   }

   @AfterAll
   public static void stopSftpServer() throws IOException {
      server.stop(true);

      try {
         System.out.printf("Deleting test storage path %s%n", TEST_STORAGE_PATH);

         FileUtils.deleteDirectory(TEST_STORAGE_PATH.toFile());
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   @BeforeEach
   public void configureDocumentRepository() {
      this.documentRepository = newSftpDocumentRepository(sftpSession);
   }

   @Override
   public DocumentRepository documentRepository() {
      return documentRepository;
   }

   private SftpDocumentRepository newSftpDocumentRepository(SessionFactory<LsEntry> session) {
      return new SftpDocumentRepository(new SftpDocumentConfigPoint(), session, new UtcDateTimeConverter());
   }

   private static SessionFactory<LsEntry> sftpSession(SshServer server) {
      DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory(false);
      factory.setHost("localhost");
      factory.setPort(server.getPort());
      factory.setUser("user");
      factory.setPassword("password");
      factory.setAllowUnknownKeys(true);

      return new CachingSessionFactory<>(factory);
   }

}