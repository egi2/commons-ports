/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.util.MimeType;

import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification.DocumentEntrySpecificationBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;

public abstract class CommonDocumentRepositoryTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeAll
   public static void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   public abstract DocumentRepository documentRepository();

   @Test
   public void testFindDocumentByIdWhenNominal() {
      Path documentPath = Path.of("path/subpath/test.pdf");
      DocumentId testDocument = new DocumentId(documentPath);

      try {
         createDocuments(documentPath);

         Assertions
               .assertThat(documentRepository().findDocumentById(testDocument))
               .hasValueSatisfying(document -> {
                  assertThat(document)
                        .usingComparator(documentComparator())
                        .isEqualTo(new DocumentBuilder()
                                         .documentId(testDocument)
                                         .streamContent("content")
                                         .metadata(new DocumentMetadataBuilder()
                                                         .reconstitute()
                                                         .documentName(documentPath)
                                                         .contentSize(21L)
                                                         .contentType(MimeType.valueOf("application/pdf"),
                                                                      StandardCharsets.UTF_8)
                                                         .chain(synchronizeMetadataDates(document.metadata()))
                                                         .build())
                                         .build());

                  System.out.printf("Document creationDate = %s%n", document.metadata().creationDate());
                  assertThat(document.metadata().creationDate().orElse(null)).satisfiesAnyOf(cd -> assertThat(
                                                                                                   cd).isNull(),
                                                                                             cd -> assertThat(
                                                                                                   cd).isAfter(
                                                                                                   ApplicationClock
                                                                                                         .nowAsInstant()
                                                                                                         .minusSeconds(
                                                                                                               3600)));
                  System.out.printf("Document lastUpdateDate = %s%n", document.metadata().lastUpdateDate());
                  assertThat(document
                                   .metadata()
                                   .lastUpdateDate()
                                   .orElse(null)).satisfiesAnyOf(cd -> assertThat(cd).isNull(),
                                                                 cd -> assertThat(cd).isAfter(ApplicationClock
                                                                                                    .nowAsInstant()
                                                                                                    .minusSeconds(
                                                                                                          3600)));
               });

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testFindDocumentByIdWhenNotFound() {
      DocumentId testDocument = new DocumentId(Path.of("path/unknown.pdf"));

      Assertions.assertThat(documentRepository().findDocumentById(testDocument)).isEmpty();
   }

   @Test
   public void testFindDocumentsBySpecificationWhenNominal() {
      String[] createDocuments = new String[] {
            "rootfile1",
            "rootfile2",
            "path/pathfile1",
            "path/pathfile2",
            "path/subpath/subpathfile1",
            "path/subpath/subpathfile2" };

      try {
         createDocuments(createDocuments);

         List<Document> documents = documentRepository()
               .findDocumentsBySpecification(new DocumentEntrySpecificationBuilder()
                                                   .addDirectories(Path.of("path"))
                                                   .build())
               .collect(toList());

         Assertions
               .assertThat(documents)
               .usingElementComparator(documentComparator())
               .containsExactlyInAnyOrder(stubDocument("path/pathfile1").metadata(synchronizeMetadataDates(
                                                filterDocument(documents.stream(), new DocumentId("path/pathfile1"))
                                                      .map(Document::metadata)
                                                      .orElse(null))),
                                          stubDocument("path/pathfile2").metadata(synchronizeMetadataDates(
                                                filterDocument(documents.stream(),
                                                               new DocumentId("path/pathfile2"))
                                                      .map(Document::metadata)
                                                      .orElse(null))));
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testSaveDocumentWhenNominal() {
      Path documentPath = Path.of("path/subpath/test.pdf");
      DocumentId testDocument = new DocumentId(documentPath);

      deleteDocuments(documentPath);

      try {
         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .loadedContent("content")
                                                            .build(), true)).isTrue();

         Assertions
               .assertThat(documentRepository().findDocumentById(testDocument))
               .hasValueSatisfying(document -> {
                  assertThat(document)
                        .usingComparator(documentComparator())
                        .isEqualTo(new DocumentBuilder()
                                         .documentId(testDocument)
                                         .streamContent("content")
                                         .metadata(new DocumentMetadataBuilder()
                                                         .reconstitute()
                                                         .documentName(documentPath)
                                                         .contentType(MimeType.valueOf("application/pdf"),
                                                                      StandardCharsets.UTF_8)
                                                         .contentSize(7L)
                                                         .chain(synchronizeMetadataDates(document.metadata()))
                                                         .build())
                                         .build());
               });

      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testSaveDocumentWhenOverwrite() {
      Path documentPath = Path.of("path/subpath/test.pdf");
      DocumentId testDocument = new DocumentId(documentPath);

      deleteDocuments(documentPath);

      Assertions.assertThat(documentRepository().findDocumentById(testDocument)).isEmpty();

      try {
         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .loadedContent("content")
                                                            .build(), true)).isTrue();

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .loadedContent("content")
                                                            .build(), true)).isTrue();

         assertThat(documentRepository().saveDocument(new DocumentBuilder()
                                                            .documentId(testDocument)
                                                            .loadedContent("content")
                                                            .build(), false)).isFalse();
      } finally {
         deleteDocuments(documentPath);
      }

   }

   @Test
   public void testFindDocumentEntryByIdWhenNominal() {
      Path documentPath = Path.of("path/subpath/test.pdf");
      DocumentId testDocument = new DocumentId(documentPath);

      try {
         createDocuments(documentPath);

         Optional<DocumentEntry> document = documentRepository().findDocumentEntryById(testDocument);

         Assertions
               .assertThat(document)
               .hasValue(new DocumentEntryBuilder()
                               .reconstitute()
                               .documentId(testDocument)
                               .metadata(new DocumentMetadataBuilder()
                                               .reconstitute()
                                               .documentName(documentPath)
                                               .contentSize(21L)
                                               .contentType(StandardCharsets.UTF_8)
                                               .chain(synchronizeMetadataDates(document
                                                                                     .map(DocumentEntry::metadata)
                                                                                     .orElse(null)))
                                               .build())
                               .build());
      } finally {
         deleteDocuments(documentPath);
      }
   }

   @Test
   public void testFindDocumentEntryByIdWhenNotFound() {
      Path documentPath = Path.of("path/subpath/test.pdf");
      DocumentId testDocument = new DocumentId(documentPath);

      deleteDocuments(documentPath);

      Optional<DocumentEntry> document = documentRepository().findDocumentEntryById(testDocument);

      Assertions.assertThat(document).isEmpty();
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenNominal() {
      String[] createDocuments = new String[] {
            "rootfile1",
            "rootfile2",
            "path/pathfile1",
            "path/pathfile2",
            "path/subpath/subpathfile1",
            "path/subpath/subpathfile2" };

      try {
         createDocuments(createDocuments);

         List<DocumentEntry> documentEntries = documentRepository()
               .findDocumentEntriesBySpecification(new DocumentEntrySpecificationBuilder()
                                                         .addDirectories(Path.of("path"))
                                                         .build())
               .collect(toList());

         Assertions
               .assertThat(documentEntries)
               .containsExactlyInAnyOrder(stubDocument("path/pathfile1")
                                                .metadata(synchronizeMetadataDates(filterDocumentEntry(
                                                      documentEntries.stream(),
                                                      new DocumentId("path/pathfile1"))
                                                                                         .map(DocumentEntry::metadata)
                                                                                         .orElse(null)))
                                                .documentEntry(),
                                          stubDocument("path/pathfile2")
                                                .metadata(synchronizeMetadataDates(filterDocumentEntry(
                                                      documentEntries.stream(),
                                                      new DocumentId("path/pathfile2"))
                                                                                         .map(DocumentEntry::metadata)
                                                                                         .orElse(null)))
                                                .documentEntry());
      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenDocumentEntrySpecificationWithNoDirectory() {
      String[] createDocuments = new String[] {
            "rootfile1",
            "rootfile2",
            "path/pathfile1",
            "path/pathfile2",
            "path/subpath/subpathfile1",
            "path/subpath/subpathfile2" };

      try {
         createDocuments(createDocuments);

         Stream<DocumentEntry> documentEntries =
               documentRepository().findDocumentEntriesBySpecification(new DocumentEntrySpecificationBuilder().build());

         Assertions
               .assertThat(documentEntries)
               .extracting(entry -> entry.documentId().idString())
               .containsExactlyInAnyOrder("rootfile1",
                                          "rootfile2",
                                          "path/pathfile1",
                                          "path/pathfile2",
                                          "path/subpath/subpathfile1",
                                          "path/subpath/subpathfile2");

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenDocumentEntrySpecificationWithEmptyDirectory() {
      String[] createDocuments = new String[] {
            "rootfile1",
            "rootfile2",
            "path/pathfile1",
            "path/pathfile2",
            "path/subpath/subpathfile1",
            "path/subpath/subpathfile2" };

      try {
         createDocuments(createDocuments);

         Stream<DocumentEntry> documentEntries =
               documentRepository().findDocumentEntriesBySpecification(new DocumentEntrySpecificationBuilder()
                                                                             .addDirectories(Path.of(""))
                                                                             .build());

         Assertions
               .assertThat(documentEntries)
               .extracting(entry -> entry.documentId().idString())
               .containsExactlyInAnyOrder("rootfile1", "rootfile2");

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenDocumentEntrySpecificationWithDirectoriesAndExtensions() {
      String[] createDocuments = new String[] {
            "rootfile1.ext1",
            "rootfile2.ext2",
            "path/pathfile1.ext1",
            "path/pathfile2.ext2",
            "path/subpath/subpathfile1.ext1",
            "path/subpath/subpathfile2.ext2" };

      try {
         createDocuments(createDocuments);

         Stream<DocumentEntry> documentEntries =
               documentRepository().findDocumentEntriesBySpecification(new DocumentEntrySpecificationBuilder()
                                                                             .addDirectories(Path.of("path"))
                                                                             .addExtensions("ext1")
                                                                             .build());

         Assertions
               .assertThat(documentEntries)
               .extracting(entry -> entry.documentId().idString())
               .containsExactlyInAnyOrder("path/pathfile1.ext1");

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenExtendingFilteringDocumentEntrySpecification() {
      String[] createDocuments = new String[] {
            "rootfile1.ext1",
            "rootfile2.ext2",
            "path/pathfile1.ext1",
            "path/pathfile2.ext2",
            "path/subpath/subpathfile1.ext1",
            "path/subpath/subpathfile2.ext2" };

      try {
         createDocuments(createDocuments);

         Stream<DocumentEntry> documentEntries =
               documentRepository().findDocumentEntriesBySpecification(new FilteringDocumentEntrySpecification() {
                  @Override
                  public List<Path> directories() {
                     return List.of(Path.of("path"));
                  }

                  @Override
                  public boolean satisfiedBy(DocumentEntry entry) {
                     return (entry.documentId().sameValueAs(new DocumentId("path/pathfile1.ext1")) || entry
                           .documentId()
                           .value()
                           .toString()
                           .contains("/subpath/") || entry.metadata().contentSize().orElse(0L) < 16) && entry
                                  .documentId()
                                  .value()
                                  .toString()
                                  .endsWith(".ext1");
                  }
               });

         Assertions
               .assertThat(documentEntries)
               .extracting(entry -> entry.documentId().idString())
               .containsExactlyInAnyOrder("path/pathfile1.ext1");

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   @Test
   public void testFindDocumentEntriesBySpecificationWhenArbitrarySpecification() {
      String[] createDocuments = new String[] {
            "rootfile1.ext1",
            "rootfile2.ext2",
            "path/pathfile1.ext1",
            "path/pathfile2.ext2",
            "path/subpath/subpathfile1.ext1",
            "path/subpath/subpathfile2.ext2" };

      try {
         createDocuments(createDocuments);

         Stream<DocumentEntry> documentEntries =
               documentRepository().findDocumentEntriesBySpecification(entry -> (entry
                                                                                       .documentId()
                                                                                       .sameValueAs(new DocumentId(
                                                                                             "path/pathfile1.ext1"))
                                                                                 || entry
                                                                                       .documentId()
                                                                                       .value()
                                                                                       .toString()
                                                                                       .contains("/subpath/")
                                                                                 || entry
                                                                                          .metadata()
                                                                                          .contentSize()
                                                                                          .orElse(0L) < 16)
                                                                                && entry
                                                                                      .documentId()
                                                                                      .value()
                                                                                      .toString()
                                                                                      .endsWith(".ext1"));

         Assertions
               .assertThat(documentEntries)
               .extracting(entry -> entry.documentId().idString())
               .containsExactlyInAnyOrder("rootfile1.ext1",
                                          "path/pathfile1.ext1",
                                          "path/subpath/subpathfile1.ext1");

      } finally {
         deleteDocuments(createDocuments);
      }
   }

   private Document stubDocument(DocumentId documentId) {
      String content = documentId.idString();

      return new DocumentBuilder()
            .documentId(documentId)
            .streamContent(content)
            .build()
            .metadata(builder -> builder
                  .contentSize((long) content.length())
                  .contentType(MimeType.valueOf("*/*;charset=UTF-8")));
   }

   private Document stubDocument(String documentId) {
      return stubDocument(new DocumentId(documentId));
   }

   private UnaryOperator<DocumentMetadataBuilder> synchronizeMetadataDates(DocumentMetadata documentMetadata) {
      return builder -> builder
            .reconstitute(true)
            .creationDate(documentMetadata.creationDate().orElse(null))
            .lastUpdateDate(documentMetadata.lastUpdateDate().orElse(null))
            .reconstitute(false);
   }

   private Optional<Document> filterDocument(Stream<Document> documents, DocumentId documentId) {
      return documents.filter(d -> d.documentId().equals(documentId)).findAny();
   }

   private Optional<DocumentEntry> filterDocumentEntry(Stream<DocumentEntry> documents,
                                                       DocumentId documentId) {
      return documents.filter(d -> d.documentId().equals(documentId)).findAny();
   }

   protected void createDocuments(String... paths) {
      createDocuments(Stream.of(paths).map(Path::of).collect(toList()));
   }

   protected void createDocuments(Path... paths) {
      createDocuments(Arrays.asList(paths));
   }

   protected void createDocuments(List<Path> paths) {
      for (Path documentPath : paths) {
         DocumentId documentId = new DocumentId(documentPath);
         documentRepository().saveDocument(stubDocument(documentId), true);
      }
   }

   protected void deleteDocuments(String... paths) {
      deleteDocuments(Stream.of(paths).map(Path::of).collect(toList()));
   }

   protected void deleteDocuments(Path... paths) {
      deleteDocuments(Arrays.asList(paths));
   }

   protected void deleteDocuments(List<Path> paths) {
      for (Path documentPath : paths) {
         DocumentId documentId = new DocumentId(documentPath);
         documentRepository().deleteDocument(documentId);
      }
   }

   private Comparator<Document> documentComparator() {
      return (d1, d2) -> d1.sameValueAs(d2) ? 0 : -1;
   }

}