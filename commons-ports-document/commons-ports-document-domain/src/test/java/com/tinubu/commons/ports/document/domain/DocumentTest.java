/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.util.MimeType;

import com.tinubu.commons.ddd.invariant.InvariantValidationException;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;

public class DocumentTest {

   @Test
   public void testDocumentWhenNominal() {
      Document document =
            new DocumentBuilder().documentId(new DocumentId("path/document.pdf")).loadedContent("").build();

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo(Path.of("path/document.pdf"));
      assertThat(document.metadata().documentFileName()).isEqualTo("document.pdf");
      assertThat(document
                       .metadata()
                       .contentType()).isEqualTo(MimeType.valueOf("application/pdf;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(0L);
      assertThat(document.content().contentSize()).hasValue(0L);
      assertThat(document.content().contentEncoding()).isEqualTo(Charset.defaultCharset());
   }

   @Test
   public void testDocumentWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder().build())
            .withMessage(
                  "Invariant validation error in [Document[documentId=<null>,metadata=<null>,content=<null>]] context : "
                  + "{documentId} 'documentId' must not be null | {content} 'content' must not be null");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder().documentId(new DocumentId("/")).build())
            .withMessage(
                  "Invariant validation error in [DocumentId[value=/,relativePath=true,newObject=false]] context : {value} 'value=/' must have a filename component");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder().documentId(new DocumentId(" ")).build())
            .withMessage(
                  "Invariant validation error in [DocumentId[value= ,relativePath=true,newObject=false]] context : {value} 'value.fileName' must not be blank");
   }

   @Test
   public void testDocumentMetadataWhenPath() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path/document.pdf"))
            .metadata(new DocumentMetadataBuilder()
                            .documentName("document2.pdf")
                            .contentType(StandardCharsets.UTF_8)
                            .build())
            .loadedContent("")
            .build();

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo(Path.of("document2.pdf"));
      assertThat(document.metadata().documentFileName()).isEqualTo("document2.pdf");
      assertThat(document
                       .metadata()
                       .contentType()).isEqualTo(MimeType.valueOf("application/pdf;charset=UTF-8"));
   }

   @Test
   public void testDocumentSetLogicalName() {
      Document document =
            new DocumentBuilder().documentId(new DocumentId("path/document.pdf")).loadedContent("").build();

      document = document.documentName(Path.of("document.txt"));

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo(Path.of("document.txt"));
      assertThat(document.metadata().documentFileName()).isEqualTo("document.txt");
      assertThat(document
                       .metadata()
                       .contentType()).isEqualTo(MimeType.valueOf("application/pdf;charset=UTF-8"));
   }

   @Test
   public void testDocumentSetContentType() {
      Document document =
            new DocumentBuilder().documentId(new DocumentId("path/document.pdf")).loadedContent("").build();

      document = document.contentType(MimeType.valueOf("application/text"));

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo(Path.of("path/document.pdf"));
      assertThat(document.metadata().documentFileName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).isEqualTo(MimeType.valueOf(
            "application/text;charset=UTF-8"));

      document = document.contentType(MimeType.valueOf("application/text;charset=UTF-8"));

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo(Path.of("path/document.pdf"));
      assertThat(document.metadata().documentFileName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).isEqualTo(MimeType.valueOf(
            "application/text;charset=UTF-8"));
   }

   @Test
   public void testDocumentSetContentTypeWhenUnmatchingContentType() {
      Document document =
            new DocumentBuilder().documentId(new DocumentId("path/document.pdf")).loadedContent("").build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.contentType(MimeType.valueOf("application/text;charset=US-ASCII")))
            .withMessage(
                  "Invariant validation error in [DocumentId[value=path/document.pdf,relativePath=true,newObject=false]] context : "
                  + "{metadata} 'metadata.contentType.charset=US-ASCII' must be equal to 'content.contentEncoding=UTF-8'");
   }

   @Test
   public void testDocumentSetAttributes() {
      Document document =
            new DocumentBuilder().documentId(new DocumentId("path/document.pdf")).loadedContent("").build();

      document = document.attributes(Map.of("k1", 43, "k2", "value2"));

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document.pdf"));
      assertThat(document.metadata().attributes()).containsOnly(Pair.of("k1", 43), Pair.of("k2", "value2"));

   }

   @Test
   public void testDocumentAddAttributes() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path/document.pdf"))
            .loadedContent("")
            .build()
            .attributes(Map.of("k1", 43, "k2", "value2"));

      document = document.addAttributes(Map.of("k3", true));

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document.pdf"));
      assertThat(document.metadata().attributes()).containsOnly(Pair.of("k1", 43),
                                                                Pair.of("k2", "value2"),
                                                                Pair.of("k3", true));

   }

   @Test
   public void testDocumentAddAttribute() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path/document.pdf"))
            .loadedContent("")
            .build()
            .attributes(Map.of("k1", 43, "k2", "value2"));

      document = document.addAttribute("k3", true);

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document.pdf"));
      assertThat(document.metadata().attributes()).containsOnly(Pair.of("k1", 43),
                                                                Pair.of("k2", "value2"),
                                                                Pair.of("k3", true));

   }

   @Test
   public void testDocumentSetMetadataWhenNominal() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path/document.pdf"))
            .loadedContent("1234")
            .build();

      document = document.metadata(new DocumentMetadataBuilder()
                                         .documentName(Path.of("document2.pdf"))
                                         .contentType(MimeType.valueOf("application/text"),
                                                      StandardCharsets.UTF_8)
                                         .build());

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo(Path.of("document2.pdf"));
      assertThat(document.metadata().documentFileName()).isEqualTo("document2.pdf");
      assertThat(document.metadata().contentType()).isEqualTo(MimeType.valueOf(
            "application/text;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(4L);
   }

   @Test
   public void testDocumentMetadataWhenUnmatchingContentSize() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder()
                  .documentId(new DocumentId("path/document.pdf"))
                  .loadedContent("content")
                  .metadata(new DocumentMetadataBuilder()
                                  .documentName(Path.of("document.pdf"))
                                  .contentType(MimeType.valueOf("application/pdf"), StandardCharsets.UTF_8)
                                  .contentSize(2L)
                                  .build())
                  .build())
            .withMessage(
                  "Invariant validation error in [DocumentId[value=path/document.pdf,relativePath=true,newObject=false]] context : "
                  + "{metadata} 'metadata.contentSize=2' must be equal to 'content.contentSize=7'");
   }

   @Test
   public void testDocumentSetMetadataWhenUnmatchingContentSize() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path/document.pdf"))
            .loadedContent("content")
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.metadata(new DocumentMetadataBuilder()
                                                      .documentName(Path.of("document.pdf"))
                                                      .contentType(MimeType.valueOf("application/pdf"),
                                                                   StandardCharsets.UTF_8)
                                                      .contentSize(2L)
                                                      .build()))
            .withMessage(
                  "Invariant validation error in [DocumentId[value=path/document.pdf,relativePath=true,newObject=false]] context : "
                  + "{metadata} 'metadata.contentSize=2' must be equal to 'content.contentSize=7'");
   }

   @Test
   public void testDocumentMetadataWhenUnmatchingContentTypeCharset() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentBuilder()
                  .documentId(new DocumentId("path/document.pdf"))
                  .loadedContent("content")
                  .metadata(new DocumentMetadataBuilder()
                                  .documentName(Path.of("document.pdf"))
                                  .contentType(MimeType.valueOf("application/pdf;charset=US-ASCII"))
                                  .build())
                  .build())
            .withMessage(
                  "Invariant validation error in [DocumentId[value=path/document.pdf,relativePath=true,newObject=false]] context : "
                  + "{metadata} 'metadata.contentType.charset=US-ASCII' must be equal to 'content.contentEncoding=UTF-8'");
   }

   @Test
   public void testDocumentSetMetadataWhenUnmatchingContentTypeCharset() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path/document.pdf"))
            .loadedContent("content")
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.metadata(new DocumentMetadataBuilder()
                                                      .documentName(Path.of("document.pdf"))
                                                      .contentType(MimeType.valueOf(
                                                            "application/pdf;charset=US-ASCII"))
                                                      .build()))
            .withMessage(
                  "Invariant validation error in [DocumentId[value=path/document.pdf,relativePath=true,newObject=false]] context : "
                  + "{metadata} 'metadata.contentType.charset=US-ASCII' must be equal to 'content.contentEncoding=UTF-8'");
   }

   @Test
   public void testDocumentMetadataWhenNotSetAndUnavailableContentSize() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path", "document.pdf"))
            .streamContent("content")
            .build();

      assertThat(document.metadata().contentSize()).isEmpty();
   }

   @Test
   public void testDocumentMetadataWhenSetAndUnavailableContentSize() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path/document.pdf"))
            .streamContent("content")
            .metadata(new DocumentMetadataBuilder()
                            .documentName("document2.pdf")
                            .contentType(StandardCharsets.UTF_8)
                            .contentSize(42L)
                            .build())
            .build();

      assertThat(document.metadata().contentSize()).hasValue(42L);
   }

   @Test
   public void testDocumentWhenSetContent() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path/document.pdf"))
            .loadedContent("content")
            .build();

      document = document.content(new LoadedDocumentContentBuilder().content("new content").build());

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo(Path.of("path/document.pdf"));
      assertThat(document.metadata().documentFileName()).isEqualTo("document.pdf");
      assertThat(document
                       .metadata()
                       .contentType()).isEqualTo(MimeType.valueOf("application/pdf;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(11L);
      assertThat(document.content().stringContent()).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).isEqualTo(Charset.defaultCharset());
   }

   @Test
   public void testDocumentWhenSetContentWithDifferentEncoding() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path/document.pdf"))
            .loadedContent("content")
            .build();

      document = document.content(new LoadedDocumentContentBuilder()
                                        .content(new ByteArrayInputStream("new content".getBytes(
                                              StandardCharsets.US_ASCII)), StandardCharsets.US_ASCII)
                                        .build());

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo(Path.of("path/document.pdf"));
      assertThat(document.metadata().documentFileName()).isEqualTo("document.pdf");
      assertThat(document.metadata().contentType()).isEqualTo(MimeType.valueOf(
            "application/pdf;charset=US-ASCII"));
      assertThat(document.metadata().contentSize()).hasValue(11L);
      assertThat(document.content().stringContent()).isEqualTo("new content");
      assertThat(document.content().contentEncoding()).isEqualTo(StandardCharsets.US_ASCII);
   }

   @Test
   public void testDocumentDuplicateWhenNominal() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path/document.pdf"))
            .metadata(new DocumentMetadataBuilder()
                            .documentName("logicalPath/document.pdf")
                            .contentType(StandardCharsets.UTF_8)
                            .build())
            .loadedContent("content1")
            .build();

      document = document.duplicate(new DocumentId("path/document2.pdf"));

      assertThat(document.documentId()).isEqualTo(new DocumentId("path/document2.pdf"));
      assertThat(document.metadata().documentName()).isEqualTo(Path.of("logicalPath/document.pdf"));
      assertThat(document.metadata().documentFileName()).isEqualTo("document.pdf");
      assertThat(document
                       .metadata()
                       .contentType()).isEqualTo(MimeType.valueOf("application/pdf;charset=UTF-8"));
      assertThat(document.metadata().contentSize()).hasValue(8L);
      assertThat(document.content().stringContent()).isEqualTo("content1");
      assertThat(document.content().contentEncoding()).isEqualTo(Charset.defaultCharset());
   }

   @Test
   public void testDocumentDuplicateWhenSameDocumentId() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path/document.pdf"))
            .loadedContent("content1")
            .build();

      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> document.duplicate(new DocumentId("path/document.pdf")))
            .withMessage("Invariant validation error : "
                         + "'newDocumentId=DocumentId[value=path/document.pdf,relativePath=true,newObject=false]' must not be equal to 'documentId=DocumentId[value=path/document.pdf,relativePath=true,newObject=false]'");
   }

   @Test
   public void testDocumentToStringWhenHiddenContent() {
      Document document = new DocumentBuilder()
            .documentId(new DocumentId("path", "document.pdf"))
            .content(new LoadedDocumentContentBuilder().content("A long or secret content").build())
            .build();

      assertThat(document.toString()).contains("DocumentContent[content=<hidden-value>");
   }

}