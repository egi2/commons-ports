/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.spy;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.util.MimeType;

import com.tinubu.commons.ddd.domain.specification.Specification;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;

public class DocumentRepositoryTest {

   @Test
   public void testTransferDocumentWhenNominal() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions
            .assertThat(source.transferDocument(new DocumentId("path/file1"), target, false))
            .isEqualTo(optional(newDocumentEntry("path/file1")));
   }

   @Test
   public void testTransferDocumentWhenBadParameters() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocument(null, target, false))
            .withMessage("'documentId' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocument(new DocumentId("path/file1"), null, false))
            .withMessage("'targetDocumentRepository' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocument(new DocumentId("path/file1"), target, false, null))
            .withMessage("'targetDocumentId' must not be null");
   }

   @Test
   public void testTransferDocumentWhenDocumentNotFound() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions
            .assertThat(source.transferDocument(new DocumentId("path/notexist"), target, false))
            .isEmpty();
   }

   @Test
   public void testTransferDocumentWhenExistingTarget() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions
            .assertThat(source.transferDocument(new DocumentId("path/exist"), target, true))
            .isEqualTo(optional(newDocumentEntry("path/exist")));
      Assertions.assertThat(source.transferDocument(new DocumentId("path/exist"), target, false)).isEmpty();
   }

   @Test
   public void testTransferDocumentWhenCustomTargetDocumentId() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions
            .assertThat(source.transferDocument(new DocumentId("path/exist"),
                                                target,
                                                true,
                                                entry -> new DocumentId(entry.documentId().value()
                                                                        + "-transferred")))
            .isEqualTo(optional(newTransferredDocumentEntry("path/exist", "path/exist-transferred")));
   }

   @Test
   public void testTransferDocumentsWhenNominal() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions
            .assertThat(source.transferDocuments(__ -> true, target, false))
            .containsExactly(newDocumentEntry("path/file1"), newDocumentEntry("path/file2"));
   }

   @Test
   public void testTransferDocumentsWhenBadParameters() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocuments(null, target, false))
            .withMessage("'documentSpecification' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocuments(__ -> true, null, false))
            .withMessage("'targetDocumentRepository' must not be null");
      assertThatNullPointerException()
            .isThrownBy(() -> source.transferDocuments(__ -> true, target, false, null))
            .withMessage("'targetDocumentId' must not be null");
   }

   @Test
   public void testTransferDocumentsWhenCustomTargetDocumentId() {
      DocumentRepository source = mockSourceRepository();
      DocumentRepository target = mockTargetRepository();

      Assertions
            .assertThat(source.transferDocuments(__ -> true,
                                                 target,
                                                 false,
                                                 entry -> new DocumentId(entry.documentId().value()
                                                                         + "-transferred")))
            .containsExactly(newTransferredDocumentEntry("path/file1", "path/file1-transferred"),
                             newTransferredDocumentEntry("path/file2", "path/file2-transferred"));
   }

   private DocumentRepository mockSourceRepository() {
      DocumentRepository sourceRepository = spy(DocumentRepository.class);

      List<DocumentEntry> documents = List.of(newDocumentEntry("path/exist"),
                                              newDocumentEntry("path/file1"),
                                              newDocumentEntry("path/file2"));

      doAnswer(invocation -> documents
            .stream()
            .filter(entry -> entry.documentId().equals(invocation.<DocumentId>getArgument(0)))
            .findAny()
            .map(this::newDocument)).when(sourceRepository).findDocumentById(any());

      doAnswer(invocation -> documents
            .stream()
            .filter(invocation.<Specification<DocumentEntry>>getArgument(0))
            .map(this::newDocument)).when(sourceRepository).findDocumentsBySpecification(any());

      doAnswer(invocation -> optional())
            .when(sourceRepository)
            .findDocumentById(new DocumentId("path/notexist"));

      return sourceRepository;
   }

   private DocumentRepository mockTargetRepository() {
      DocumentRepository targetRepository = spy(DocumentRepository.class);

      doAnswer(__ -> true).when(targetRepository).saveDocument(any(), anyBoolean());
      doAnswer(__ -> true)
            .when(targetRepository)
            .saveDocument(argThat(d -> d.documentId().equals(new DocumentId("path/exist")) || d
                  .documentId()
                  .equals(new DocumentId("path/exist-transferred"))), eq(true));
      doAnswer(__ -> false)
            .when(targetRepository)
            .saveDocument(argThat(d -> d.documentId().equals(new DocumentId("path/exist")) || d
                  .documentId()
                  .equals(new DocumentId("path/exist-transferred"))), eq(false));

      return targetRepository;
   }

   private DocumentEntry newDocumentEntry(DocumentId documentId) {
      return new DocumentEntryBuilder()
            .documentId(documentId)
            .metadata(new DocumentMetadataBuilder()
                            .documentName(documentId.value())
                            .contentSize((long) documentId.idString().length())
                            .contentType(MimeType.valueOf("*/*;charset=UTF-8"))
                            .build())
            .build();
   }

   private DocumentEntry newDocumentEntry(String documentId) {
      return newDocumentEntry(new DocumentId(documentId));
   }

   private DocumentEntry newTransferredDocumentEntry(DocumentId documentId, DocumentId newDocumentId) {
      return DocumentEntryBuilder.from(newDocumentEntry(documentId)).documentId(newDocumentId).build();
   }

   private DocumentEntry newTransferredDocumentEntry(String documentId, String newDocumentId) {
      return newTransferredDocumentEntry(new DocumentId(documentId), new DocumentId(newDocumentId));
   }

   private Document newDocument(DocumentId documentId) {
      return newDocument(newDocumentEntry(documentId));
   }

   private Document newDocument(String documentId) {
      return newDocument(new DocumentId(documentId));
   }

   private Document newDocument(DocumentEntry documentEntry) {
      return new DocumentBuilder()
            .documentEntry(documentEntry)
            .loadedContent(documentEntry.documentId().idString())
            .build();
   }

}
