/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd.invariant.InvariantValidationException;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentEntrySpecification.DocumentEntrySpecificationBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;

class DocumentEntrySpecificationTest {

   @Test
   public void testDocumentPathSpecificationWhenNominal() {
      DocumentEntrySpecification specification =
            new DocumentEntrySpecificationBuilder().addDirectories(Path.of("path")).build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/subpath/file"))
                                                 .metadata(new DocumentMetadataBuilder()
                                                                 .documentName("path/subpath", "file")
                                                                 .contentType(StandardCharsets.UTF_8)
                                                                 .build())
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("otherpath/file"))
                                                 .metadata(documentMetadata("otherpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isFalse();
   }

   @Test
   public void testDocumentPathSpecificationWhenBadParameters() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new DocumentEntrySpecificationBuilder().addDirectories(Path.of("..")).build())
            .withMessage(
                  "Invariant validation error in [DocumentEntrySpecification[directories=[..],extensions=[]]] context : "
                  + "{directories} 'directories=[..]' > 'directories[0]=..' must not have traversal paths");

   }

   @Test
   public void testDocumentPathSpecificationWhenEmptyPath() {
      DocumentEntrySpecification specification =
            new DocumentEntrySpecificationBuilder().addDirectories(Path.of("")).build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("otherpath/file"))
                                                 .metadata(documentMetadata("otherpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isTrue();
   }

   @Test
   public void testDocumentPathSpecificationWhenNoDirectories() {
      DocumentEntrySpecification specification = new DocumentEntrySpecificationBuilder().build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("otherpath/file"))
                                                 .metadata(documentMetadata("otherpath/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isTrue();
   }

   @Test
   public void testDocumentPathSpecificationWhenMultipleDirectories() {
      DocumentEntrySpecification specification = new DocumentEntrySpecificationBuilder()
            .addDirectories(Path.of("path"), Path.of("otherpath"))
            .build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("otherpath/file"))
                                                 .metadata(documentMetadata("otherpath/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isFalse();
   }

   @Test
   public void testDocumentPathSpecificationWhenSubDirectories() {
      DocumentEntrySpecification specification =
            new DocumentEntrySpecificationBuilder().addDirectories(Path.of("path/subpath")).build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("otherpath/file"))
                                                 .metadata(documentMetadata("otherpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isFalse();
   }

   @Test
   public void testDocumentPathSpecificationWhenDotPath() {
      DocumentEntrySpecification specification =
            new DocumentEntrySpecificationBuilder().addDirectories(Path.of(".")).build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/subpath/file"))
                                                 .metadata(documentMetadata("path/subpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("otherpath/file"))
                                                 .metadata(documentMetadata("otherpath/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isTrue();
   }

   @Test
   public void testDocumentPathSpecificationWhenExtensions() {
      DocumentEntrySpecification specification = new DocumentEntrySpecificationBuilder()
            .addDirectories(Path.of("path"))
            .addExtensions("ext")
            .build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/file.ext"))
                                                 .metadata(documentMetadata("path/file.ext"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/file.otherext"))
                                                 .metadata(documentMetadata("path/file.otherext"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isFalse();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("file.ext"))
                                                 .metadata(documentMetadata("file.ext"))
                                                 .build())).isFalse();
   }

   @Test
   public void testDocumentPathSpecificationWhenEmptyExtension() {
      DocumentEntrySpecification specification =
            new DocumentEntrySpecificationBuilder().addExtensions("", "ext").build();

      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/file"))
                                                 .metadata(documentMetadata("path/file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("path/file.ext"))
                                                 .metadata(documentMetadata("path/file.ext"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("file"))
                                                 .metadata(documentMetadata("file"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("file.ext"))
                                                 .metadata(documentMetadata("file.ext"))
                                                 .build())).isTrue();
      assertThat(specification.satisfiedBy(new DocumentEntryBuilder()
                                                 .documentId(new DocumentId("file.otherext"))
                                                 .metadata(documentMetadata("file.otherext"))
                                                 .build())).isFalse();
   }

   private DocumentMetadata documentMetadata(String documentName) {
      return new DocumentMetadataBuilder()
            .documentName(documentName)
            .contentType(StandardCharsets.UTF_8)
            .build();
   }

}