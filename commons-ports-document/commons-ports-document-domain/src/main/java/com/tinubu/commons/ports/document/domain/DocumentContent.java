/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Optional;

/**
 * Document content attached to a {@link Document}.
 */
public interface DocumentContent {

   /** System encoding for system-based encoding. */
   Charset SYSTEM_CONTENT_ENCODING = Charset.defaultCharset();

   /**
    * Returns content as a byte array.
    *
    * @return document content
    */
   byte[] content();

   /**
    * Returns content as a string. String is encoded using {@link #contentEncoding()}.
    *
    * @return document content
    */
   String stringContent();

   /**
    * Returns content as an {@link InputStream}.
    * The stream must be closed by the client code.
    *
    * @return document content as byte array
    */
   InputStream inputStreamContent();

   /**
    * Returns content as a string {@link Reader}. Reader is encoded using {@link #contentEncoding()}.
    * The reader must be closed by the client code.
    *
    * @return document content as string reader
    */
   Reader readerContent();

   /**
    * Returns content size in bytes.
    *
    * @return document content size, or {@link Optional#empty} of loader is dynamic and content size is not
    *       known
    */
   Optional<Long> contentSize();

   /**
    * Returns document content encoding {@link Charset}.
    *
    * @return document content encoding charset
    */
   Charset contentEncoding();
}

