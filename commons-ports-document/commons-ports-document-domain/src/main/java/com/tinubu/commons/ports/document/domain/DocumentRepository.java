/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import com.tinubu.commons.ddd.domain.specification.Specification;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;

/**
 * Domain repository to access {@link Document} documents. This abstraction enable to store or retrieve
 * documents.
 * <p>
 * A {@link DocumentId} uniquely identify a document in the repository backend. It must be a relative path.
 */
public interface DocumentRepository {

   /**
    * Finds document by id.
    *
    * @param documentId document id
    *
    * @return found document or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   Optional<Document> findDocumentById(DocumentId documentId);

   /**
    * Finds document by document entry specification.
    *
    * @param specification document entry specification
    *
    * @return found documents
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   Stream<Document> findDocumentsBySpecification(Specification<DocumentEntry> specification);

   /**
    * Finds document entry by id. This enables to only retrieve metadata and not document content.
    * If document exists, a document metadata is always returned with available data.
    *
    * @param documentId document id
    *
    * @return found document entry or {@link Optional#empty()} if document not found
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   Optional<DocumentEntry> findDocumentEntryById(DocumentId documentId);

   /**
    * Finds document entries by document entry specification.
    *
    * @param specification document entry specification
    *
    * @return found document entries
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   Stream<DocumentEntry> findDocumentEntriesBySpecification(Specification<DocumentEntry> specification);

   /**
    * Saves specified document.
    *
    * @param document document to save
    * @param overwrite accept to overwrite pre-existing document with same if {@code true}
    *
    * @return {@code true} if document has been saved or {@code false} otherwise (e.g.: document with same id
    *       already exists and overwrite is not set)
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   boolean saveDocument(Document document, boolean overwrite);

   /**
    * Deletes specified document.
    *
    * @param documentId document to delete
    *
    * @return {@code true} if document has been deleted or {@code false} otherwise (e.g.: document not found)
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   boolean deleteDocument(DocumentId documentId);

   /**
    * Transfer specified document to another repository.
    *
    * @param documentId document to transfer
    * @param targetDocumentRepository target repository to transfer document to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    * @param targetDocumentId document identifier mapper for target document
    *
    * @return new document entry in target repository or {@link Optional#empty} if document not
    *       transferred. Document is considered not transferred if not found, or already existing and
    *       overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   default Optional<DocumentEntry> transferDocument(DocumentId documentId,
                                                    DocumentRepository targetDocumentRepository,
                                                    boolean overwrite,
                                                    Function<DocumentEntry, DocumentId> targetDocumentId) {
      notNull(documentId, "documentId");
      notNull(targetDocumentRepository, "targetDocumentRepository");
      notNull(targetDocumentId, "targetDocumentId");

      return findDocumentById(documentId).flatMap(document -> {
         DocumentId newDocumentId = targetDocumentId.apply(document.documentEntry());
         Document newDocument = DocumentBuilder.from(document).documentId(newDocumentId).build();

         if (targetDocumentRepository.saveDocument(newDocument, overwrite)) {
            return optional(newDocument.documentEntry());
         } else {
            return optional();
         }
      });
   }

   /**
    * Transfer specified document to another repository, with default document identifier mapper for target
    * document. Default mapper reuse source document identifier.
    *
    * @param documentId document to transfer
    * @param targetDocumentRepository target repository to transfer document to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    *
    * @return new document entry in target repository or {@link Optional#empty} if document not
    *       transferred. Document is considered not transferred if not found, or already existing and
    *       overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   default Optional<DocumentEntry> transferDocument(DocumentId documentId,
                                                    DocumentRepository targetDocumentRepository,
                                                    boolean overwrite) {
      return transferDocument(documentId, targetDocumentRepository, overwrite, DocumentEntry::documentId);
   }

   /**
    * Transfer documents satisfied by specification to another repository.
    *
    * @param documentSpecification document specification to identify documents to transfer
    * @param targetDocumentRepository target repository to transfer documents to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    * @param targetDocumentId document identifier mapper for target documents
    *
    * @return transferred document entries in target repository. Documents are considered not transferred
    *       if already existing and overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   default Stream<DocumentEntry> transferDocuments(Specification<DocumentEntry> documentSpecification,
                                                   DocumentRepository targetDocumentRepository,
                                                   boolean overwrite,
                                                   Function<DocumentEntry, DocumentId> targetDocumentId) {
      notNull(documentSpecification, "documentSpecification");
      notNull(targetDocumentRepository, "targetDocumentRepository");
      notNull(targetDocumentId, "targetDocumentId");

      return findDocumentsBySpecification(documentSpecification).flatMap(document -> {
         DocumentId newDocumentId = targetDocumentId.apply(document.documentEntry());
         Document newDocument = DocumentBuilder.from(document).documentId(newDocumentId).build();

         if (targetDocumentRepository.saveDocument(newDocument, overwrite)) {
            return Stream.of(newDocument.documentEntry());
         } else {
            return Stream.empty();
         }
      });
   }

   /**
    * Transfer documents satisfied by specification to another repository,with default document identifier
    * mapper for target document. Default mapper reuse source document identifier.
    *
    * @param documentSpecification document specification to identify documents to transfer
    * @param targetDocumentRepository target repository to transfer documents to
    * @param overwrite accept to overwrite pre-existing document with same identifier
    *
    * @return transferred document entries in target repository. Documents are considered not transferred
    *       if already existing and overwrite is not set
    *
    * @throws DocumentAccessException if unexpected document access error occurs
    */
   default Stream<DocumentEntry> transferDocuments(Specification<DocumentEntry> documentSpecification,
                                                   DocumentRepository targetDocumentRepository,
                                                   boolean overwrite) {
      return transferDocuments(documentSpecification,
                               targetDocumentRepository,
                               overwrite,
                               DocumentEntry::documentId);
   }
}
