/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import java.nio.file.Path;

import com.tinubu.commons.ddd.domain.ids.PathId;

/**
 * Uniquely identify a document in the repository. This value uniquely identifies the document in the
 * infrastructure and is not aimed to name the document, use {@link DocumentMetadata#documentName()} for that.
 * Document identification path must be relative.
 */
public class DocumentId extends PathId {
   public DocumentId(Path documentId) {
      super(documentId, true);
   }

   public DocumentId(String first, String... more) {
      this(Path.of(first, more));
   }
}
