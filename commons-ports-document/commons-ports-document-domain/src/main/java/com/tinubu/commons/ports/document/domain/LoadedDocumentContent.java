/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Optional;

import org.apache.commons.io.IOUtils;

import com.tinubu.commons.ddd.domain.model.DomainBuilder;
import com.tinubu.commons.ddd.domain.type.Fields;
import com.tinubu.commons.ddd.domain.type.Value;
import com.tinubu.commons.ddd.domain.type.support.LazyFields;
import com.tinubu.commons.ddd.valueformatter.HiddenValueFormatter;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

/**
 * Document content attached to a {@link Document}.
 */
public class LoadedDocumentContent implements DocumentContent, Value<LoadedDocumentContent> {

   /** Document content as byte array. */
   private final byte[] content;
   /** Document content encoding. */
   private final Charset contentEncoding;

   private final LazyFields<LoadedDocumentContent> domainFields = new LazyFields<>(this::defineDomainFields);

   private LoadedDocumentContent(LoadedDocumentContentBuilder builder) {
      this.content = builder.content;
      this.contentEncoding = builder.contentEncoding;

      validateInvariants().orThrow();
   }

   public Fields<LoadedDocumentContent> defineDomainFields() {
      return fieldsBuilder()
            .field("content", v -> v.content, new HiddenValueFormatter(), isNotNull())
            .field("contentEncoding", v -> v.contentEncoding, isNotNull())
            .build();
   }

   @Override
   public Fields<LoadedDocumentContent> domainFields() {
      return domainFields.get();
   }

   @Override
   public String stringContent() {
      return new String(content, contentEncoding);
   }

   @Override
   public ByteArrayInputStream inputStreamContent() {
      return new ByteArrayInputStream(content);
   }

   @Override
   public InputStreamReader readerContent() {
      return new InputStreamReader(inputStreamContent(), contentEncoding);
   }

   @Override
   public Optional<Long> contentSize() {
      return optional((long) content.length);
   }

   @Override
   @Getter
   public byte[] content() {
      return content;
   }

   @Override
   @Getter
   public Charset contentEncoding() {
      return contentEncoding;
   }

   @Override
   public boolean equals(Object o) {
      return valueEquals(o);
   }

   @Override
   public int hashCode() {
      return valueHashCode();
   }

   @Override
   public String toString() {
      return valueToString();
   }

   public static LoadedDocumentContentBuilder reconstituteBuilder() {
      return new LoadedDocumentContentBuilder().reconstitute();
   }

   public static class LoadedDocumentContentBuilder
         extends DomainBuilder<LoadedDocumentContent, LoadedDocumentContentBuilder> {
      private byte[] content;
      private Charset contentEncoding;

      public LoadedDocumentContentBuilder content(byte[] content, Charset contentEncoding) {
         this.content = content;
         this.contentEncoding = contentEncoding;

         return this;
      }

      public LoadedDocumentContentBuilder content(String content) {
         return content(content.getBytes(SYSTEM_CONTENT_ENCODING), SYSTEM_CONTENT_ENCODING);
      }

      public LoadedDocumentContentBuilder content(InputStream content, Charset contentEncoding) {
         try {
            return content(IOUtils.toByteArray(content), contentEncoding);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      public LoadedDocumentContentBuilder content(Reader content) {
         try {
            return content(IOUtils.toByteArray(content, SYSTEM_CONTENT_ENCODING), SYSTEM_CONTENT_ENCODING);
         } catch (IOException e) {
            throw new IllegalStateException(e);
         }
      }

      /**
       * Reconstitute setter. Use {@link #content(byte[], Charset)} instead.
       *
       * @param content content
       *
       * @return this builder
       */
      @Setter
      public LoadedDocumentContentBuilder content(byte[] content) {
         ensureReconstitute();

         this.content = content;

         return this;
      }

      /**
       * Reconstitute setter. Use {@link #content(byte[], Charset)} instead.
       *
       * @param contentEncoding content encoding
       *
       * @return this builder
       */
      @Setter
      public LoadedDocumentContentBuilder contentEncoding(Charset contentEncoding) {
         ensureReconstitute();

         this.contentEncoding = contentEncoding;

         return this;
      }

      public LoadedDocumentContent build() {
         return new LoadedDocumentContent(this);
      }

   }

}

