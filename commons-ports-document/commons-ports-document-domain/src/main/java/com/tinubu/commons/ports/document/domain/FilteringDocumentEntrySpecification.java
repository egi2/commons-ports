/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static java.util.Collections.emptyList;

import java.nio.file.Path;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.tinubu.commons.ddd.domain.specification.Specification;

/**
 * {@link DocumentEntry} specification, specifying a concrete list of directories and other file
 * characteristics to enable fast documents searching and filtering.
 * Extending specification can add more complex filters over this one that should be used to optimize document
 * listing.
 */
public interface FilteringDocumentEntrySpecification extends Specification<DocumentEntry> {

   default boolean satisfiedBy(DocumentEntry entry) {
      boolean satisfiesDirectory;
      if (directories().isEmpty()) {
         satisfiesDirectory = true;
      } else {
         Path entryDirectory = nullable(entry.documentId().value().getParent(), Path.of(""));

         satisfiesDirectory = directories().stream().anyMatch(entryDirectory::equals);
      }

      boolean satisfiesExtension = false;

      if (satisfiesDirectory) {
         if (extensions().isEmpty()) {
            satisfiesExtension = true;
         } else {
            String entryExtension =
                  FilenameUtils.getExtension(entry.documentId().value().getFileName().toString());

            satisfiesExtension = extensions().stream().anyMatch(entryExtension::equals);
         }
      }

      return satisfiesDirectory && satisfiesExtension;
   }

   /**
    * References a discrete list of base directories to search documents. Only documents that are directly
    * included into specified directories are matched. If no directories are specified, documents are not
    * filtered out by directories. You can list only the documents in the storage "base directory" specifying
    * an empty {@link Path}.
    *
    * @return base directories to filter
    */
   default List<Path> directories() {
      return emptyList();
   }

   /**
    * References a discrete list of filename extensions to search for. Extensions must not contain the '.'
    * separator. If no extensions are specified, documents are not filtered out by extension.
    *
    * @return file extensions to filter
    */
   default List<String> extensions() {
      return emptyList();
   }

}
