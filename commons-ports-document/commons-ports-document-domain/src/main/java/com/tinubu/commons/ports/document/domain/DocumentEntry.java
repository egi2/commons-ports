/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd.invariant.rules.BaseRules.isNotNull;

import com.tinubu.commons.ddd.domain.model.DomainBuilder;
import com.tinubu.commons.ddd.domain.type.Fields;
import com.tinubu.commons.ddd.domain.type.Value;
import com.tinubu.commons.ddd.domain.type.support.LazyFields;
import com.tinubu.commons.lang.beans.Getter;

/**
 * References an identified document with its {@link DocumentMetadata}.
 */
public class DocumentEntry implements Value<DocumentEntry> {
   private final DocumentId documentId;
   private final DocumentMetadata metadata;

   private final LazyFields<DocumentEntry> domainFields = new LazyFields<>(this::defineDomainFields);

   private DocumentEntry(DocumentEntryBuilder builder) {
      this.documentId = builder.documentId;
      this.metadata = builder.metadata;

      validateInvariants().orThrow();
   }

   public Fields<DocumentEntry> defineDomainFields() {
      return fieldsBuilder()
            .field("documentId", v -> v.documentId, isNotNull())
            .field("metadata", v -> v.metadata, isNotNull())
            .build();
   }

   @Getter
   public DocumentId documentId() {
      return documentId;
   }

   @Getter
   public DocumentMetadata metadata() {
      return metadata;
   }

   @Override
   public Fields<DocumentEntry> domainFields() {
      return domainFields.get();
   }

   @Override
   public boolean equals(Object o) {
      return valueEquals(o);
   }

   @Override
   public int hashCode() {
      return valueHashCode();
   }

   @Override
   public String toString() {
      return valueToString();
   }

   public static DocumentEntryBuilder reconstituteBuilder() {
      return new DocumentEntryBuilder().reconstitute();
   }

   public static class DocumentEntryBuilder extends DomainBuilder<DocumentEntry, DocumentEntryBuilder> {
      private DocumentId documentId;
      private DocumentMetadata metadata;

      public static DocumentEntryBuilder from(DocumentEntry documentEntry) {
         return new DocumentEntryBuilder().documentId(documentEntry.documentId).metadata(documentEntry.metadata);
      }

      public DocumentEntryBuilder documentId(DocumentId documentId) {
         this.documentId = documentId;
         return this;
      }

      public DocumentEntryBuilder metadata(DocumentMetadata metadata) {
         this.metadata = metadata;
         return this;
      }

      public DocumentEntry build() {
         return new DocumentEntry(this);
      }

   }

}
