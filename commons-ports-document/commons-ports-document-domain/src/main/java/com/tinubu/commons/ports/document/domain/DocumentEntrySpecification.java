/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd.invariant.rules.CollectionRules.hasNoNullElements;
import static com.tinubu.commons.ddd.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.lang.model.ImmutableUtils.immutableList;
import static com.tinubu.commons.lang.model.MutableUtils.mutableList;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static java.util.stream.Collectors.toList;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tinubu.commons.ddd.domain.model.DomainBuilder;
import com.tinubu.commons.ddd.domain.type.Fields;
import com.tinubu.commons.ddd.domain.type.Value;
import com.tinubu.commons.ddd.domain.type.support.LazyFields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

/**
 * A concrete implementation of {@link DocumentEntry} specification.
 */
public class DocumentEntrySpecification
      implements FilteringDocumentEntrySpecification, Value<DocumentEntrySpecification> {

   private final List<Path> directories;
   private final List<String> extensions;

   private final LazyFields<DocumentEntrySpecification> domainFields = new LazyFields<>(this::defineDomainFields);

   public DocumentEntrySpecification(DocumentEntrySpecificationBuilder builder) {
      this.directories = immutableList(normalize(builder.directories));
      this.extensions = immutableList(builder.extensions);

      validateInvariants().orThrow();
   }

   private List<Path> normalize(List<Path> directories) {
      return directories.stream().map(Path::normalize).distinct().collect(toList());
   }

   @Override
   public Fields<DocumentEntrySpecification> domainFields() {
      return domainFields.get();
   }

   public Fields<DocumentEntrySpecification> defineDomainFields() {
      return fieldsBuilder()
            .field("directories",
                   v -> v.directories,
                   allSatisfies(isNotAbsolute().andValue(hasNoTraversal())))
            .field("extensions", v -> v.extensions, hasNoNullElements())
            .build();
   }

   @Getter
   public List<Path> directories() {
      return directories;
   }

   @Getter
   public List<String> extensions() {
      return extensions;
   }

   @Override
   public boolean equals(Object o) {
      return valueEquals(o);
   }

   @Override
   public int hashCode() {
      return valueHashCode();
   }

   @Override
   public String toString() {
      return valueToString();
   }

   public static DocumentEntrySpecificationBuilder reconstituteBuilder() {
      return new DocumentEntrySpecificationBuilder().reconstitute();
   }

   public static class DocumentEntrySpecificationBuilder
         extends DomainBuilder<DocumentEntrySpecification, DocumentEntrySpecificationBuilder> {

      private List<Path> directories = new ArrayList<>();
      private List<String> extensions = new ArrayList<>();

      @Setter
      public DocumentEntrySpecificationBuilder directories(List<Path> directories) {
         this.directories = mutableList(noNullElements(directories, "directories"));
         return this;
      }

      public DocumentEntrySpecificationBuilder addDirectories(List<Path> directories) {
         this.directories.addAll(noNullElements(directories, "directories"));
         return this;
      }

      public DocumentEntrySpecificationBuilder addDirectories(Path... directories) {
         return addDirectories(Arrays.asList(noNullElements(directories, "directories")));
      }

      @Setter
      public DocumentEntrySpecificationBuilder extensions(List<String> extensions) {
         this.extensions = mutableList(noNullElements(extensions, "extensions"));
         return this;
      }

      public DocumentEntrySpecificationBuilder addExtensions(List<String> extensions) {
         this.extensions.addAll(noNullElements(extensions, "extensions"));
         return this;
      }

      public DocumentEntrySpecificationBuilder addExtensions(String... extensions) {
         return addExtensions(Arrays.asList(noNullElements(extensions, "extensions")));
      }

      @Override
      public DocumentEntrySpecification build() {
         return new DocumentEntrySpecification(this);
      }
   }

}
