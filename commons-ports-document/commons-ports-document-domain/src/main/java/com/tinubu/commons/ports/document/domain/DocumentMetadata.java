/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd.invariant.Invariant.validate;
import static com.tinubu.commons.ddd.invariant.ParameterValue.lazyValue;
import static com.tinubu.commons.ddd.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd.invariant.rules.BaseRules.string;
import static com.tinubu.commons.ddd.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd.invariant.rules.ComparableRules.isInRangeInclusive;
import static com.tinubu.commons.ddd.invariant.rules.ComparableRules.isLessThanOrEqualTo;
import static com.tinubu.commons.ddd.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.ddd.invariant.rules.MapRules.entrySet;
import static com.tinubu.commons.ddd.invariant.rules.MapRules.hasNoNullKeyValue;
import static com.tinubu.commons.ddd.invariant.rules.PathRules.fileName;
import static com.tinubu.commons.ddd.invariant.rules.PathRules.isNotAbsolute;
import static com.tinubu.commons.ddd.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd.invariant.rules.StringRules.length;
import static com.tinubu.commons.ddd.invariant.rules.TemporalRules.isNotBefore;
import static com.tinubu.commons.lang.model.ImmutableUtils.immutableMap;
import static com.tinubu.commons.lang.model.MutableUtils.mutableMap;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static java.util.Collections.emptyMap;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.util.MimeType;

import com.tinubu.commons.ddd.domain.model.DomainBuilder;
import com.tinubu.commons.ddd.domain.type.Fields;
import com.tinubu.commons.ddd.domain.type.Value;
import com.tinubu.commons.ddd.domain.type.support.LazyFields;
import com.tinubu.commons.ddd.invariant.ParameterValue;
import com.tinubu.commons.ddd.valueformatter.StringValueFormatter;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.lang.datetime.ApplicationClock;

/**
 * Document metadata attached to a {@link Document}.
 */
public class DocumentMetadata implements Value<DocumentMetadata> {

   /**
    * Maximum document name length.
    */
   public static final int DOCUMENT_NAME_MAX_LENGTH = 255;
   /** Hard-cap content max size. */
   public static final long CONTENT_HARD_MAX_SIZE = 2L * 1024 * 1024 * 1024 * 1024;
   /** Default content-type when content type is not known and can't be auto-detected. */
   private static final MimeType DEFAULT_CONTENT_TYPE = MimeType.valueOf("*/*");

   private final Path documentName;
   private final MimeType contentType;
   private final Long contentSize;
   private final Instant creationDate;
   private final Instant lastUpdateDate;
   private final Map<String, Object> attributes;

   private final LazyFields<DocumentMetadata> domainFields = new LazyFields<>(this::defineDomainFields);

   private DocumentMetadata(DocumentMetadataBuilder builder) {
      this.documentName = builder.documentName;
      this.creationDate = builder.creationDate;
      this.lastUpdateDate = builder.lastUpdateDate;
      this.attributes = immutableMap(builder.attributes);
      this.contentSize = builder.contentSize;

      validateInvariants("dependency").orThrow();

      this.contentType = resolveContentType(builder);

      validateInvariants().orThrow();
   }

   public Fields<DocumentMetadata> defineDomainFields() {
      ParameterValue<Instant> creationDate = lazyValue(() -> this.creationDate, "creationDate");

      return fieldsBuilder()
            .field("documentName",
                   v -> v.documentName,
                   new StringValueFormatter(DOCUMENT_NAME_MAX_LENGTH).compose(DocumentMetadata::documentName),
                   isNotAbsolute().andValue(fileName(string(isNotBlank().andValue(length(isLessThanOrEqualTo(
                         value(DOCUMENT_NAME_MAX_LENGTH, "DOCUMENT_NAME_MAX_LENGTH"))))))),
                   "dependency")
            .field("contentType",
                   v -> v.contentType,
                   isNotNull().andValue(property(MimeType::getCharset, "charset", isNotNull())))
            .field("contentSize",
                   v -> v.contentSize,
                   isNull().orValue(isInRangeInclusive(value(0L),
                                                       value(CONTENT_HARD_MAX_SIZE,
                                                             "CONTENT_HARD_MAX_SIZE"))))
            .field("creationDate", v -> v.creationDate)
            .field("lastUpdateDate",
                   v -> v.lastUpdateDate,
                   isNull().orValue(isNotBefore(creationDate).ifIsSatisfied(creationDate::nonNull)))
            .field("attributes", v -> v.attributes, entrySet(allSatisfies(hasNoNullKeyValue())))
            .build();
   }

   @Override
   public Fields<DocumentMetadata> domainFields() {
      return domainFields.get();
   }

   /**
    * Document logical name.
    * This path is an abstraction of a hierarchical representation, not actually linked to a filesystem
    * representation.
    * Document name path must be relative.
    * <p>
    * It represents a logical name for this document, not a technical
    * name or identifier.
    * The underlying storage system must use the {@link Document#documentId()} for storage, not this name
    * that is not guaranteed to be unique.
    *
    * @return document name
    */
   @Getter
   public Path documentName() {
      return documentName;
   }

   /**
    * Returns document name, without relative path if any.
    *
    * @return name of document
    */
   public String documentFileName() {
      return documentName.getFileName().toString();
   }

   /**
    * Checks and resolves document content-type from builder parameters.
    * Resulting {@link MimeType} includes charset parameter for content encoding if possible.
    * If charset is set in both contentType and contentEncoding, the value must match.
    *
    * @param builder builder
    *
    * @return resolved content-type, never {@code null}
    */
   private MimeType resolveContentType(DocumentMetadataBuilder builder) {
      MimeType mimeType;

      if (builder.contentType == null) {
         mimeType = autoDetectMimeType(documentFileName()).orElse(DEFAULT_CONTENT_TYPE);
      } else {
         mimeType = builder.contentType;
      }

      if (mimeType.getCharset() == null && builder.contentEncoding != null) {
         mimeType = new MimeType(mimeType, builder.contentEncoding);
      }

      ParameterValue<Object> contentEncoding = value(builder.contentEncoding, "contentEncoding");
      validate(mimeType.getCharset(),
               "contentType.charset",
               isNull().orValue(isEqualTo(contentEncoding).ifIsSatisfied(contentEncoding::nonNull))).orThrow();

      return mimeType;
   }

   /**
    * Try to resolve mime-type from document name's extension.
    *
    * @param fileName document file name
    *
    * @return resolved mime-type or {@link Optional#empty()}
    */
   private static Optional<MimeType> autoDetectMimeType(String fileName) {
      try {
         return nullable(Files.probeContentType(Path.of(fileName))).map(MimeType::valueOf);
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Document content type.
    * Returns {@link #DEFAULT_CONTENT_TYPE} if content type is unknown.
    *
    * @return document mime type
    */
   @Getter
   public MimeType contentType() {
      return contentType;
   }

   /**
    * Document content type without parameters (charset, ...), as a simple type + subtype mime type.
    *
    * @return document mime type without parameters (charset, ...)
    */
   public MimeType simpleContentType() {
      return new MimeType(contentType, emptyMap());
   }

   /**
    * Content size in bytes if available
    *
    * @return content size or {@link Optional#empty} if content size is not available
    */
   @Getter
   public Optional<Long> contentSize() {
      return nullable(contentSize);
   }

   @Getter
   public Optional<Instant> creationDate() {
      return nullable(creationDate);
   }

   @Getter
   public Optional<Instant> lastUpdateDate() {
      return nullable(lastUpdateDate);
   }

   /**
    * Document extra attributes.
    *
    * @return document extra attributes
    */
   @Getter
   public Map<String, Object> attributes() {
      return attributes;
   }

   /**
    * Document extra attribute value for specified key.
    *
    * @param key attribute key
    *
    * @return attribute value or {@link Optional#empty} if attribute not set
    */
   public Optional<Object> attributes(String key) {
      return nullable(attributes.get(key));
   }

   @Override
   public boolean equals(Object o) {
      return valueEquals(o);
   }

   @Override
   public int hashCode() {
      return valueHashCode();
   }

   @Override
   public String toString() {
      return valueToString();
   }

   public static DocumentMetadataBuilder reconstituteBuilder() {
      return new DocumentMetadataBuilder().reconstitute();
   }

   public static class DocumentMetadataBuilder
         extends DomainBuilder<DocumentMetadata, DocumentMetadataBuilder> {
      private Path documentName;
      private MimeType contentType;
      private Charset contentEncoding;
      private Long contentSize;
      private Instant creationDate;
      private Instant lastUpdateDate;
      private Map<String, Object> attributes = new HashMap<>();

      public static DocumentMetadataBuilder from(DocumentMetadata metadata) {
         return new DocumentMetadataBuilder()
               .documentName(metadata.documentName)
               .contentType(metadata.contentType)
               .contentSize(metadata.contentSize)
               .attributes(metadata.attributes)
               .reconstitute(true)
               .creationDate(metadata.creationDate)
               .lastUpdateDate(metadata.lastUpdateDate)
               .reconstitute(false);
      }

      public static DocumentMetadataBuilder newDocument() {
         Instant now = ApplicationClock.nowAsInstant();

         return new DocumentMetadataBuilder()
               .reconstitute(true)
               .creationDate(now)
               .lastUpdateDate(now)
               .reconstitute(false);
      }

      @Setter
      public DocumentMetadataBuilder documentName(Path documentName) {
         this.documentName = documentName;
         return this;
      }

      public DocumentMetadataBuilder documentName(String documentName, String... moreDocumentName) {
         this.documentName = Paths.get(documentName, moreDocumentName);
         return this;
      }

      @Setter
      public DocumentMetadataBuilder contentType(MimeType contentType) {
         this.contentType = contentType;
         return this;
      }

      public DocumentMetadataBuilder contentType(MimeType contentType, Charset contentEncoding) {
         this.contentType = contentType;
         this.contentEncoding = contentEncoding;
         return this;
      }

      public DocumentMetadataBuilder contentType(Charset contentEncoding) {
         this.contentEncoding = contentEncoding;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder contentSize(Long contentSize) {
         this.contentSize = contentSize;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder creationDate(Instant creationDate) {
         ensureReconstitute();
         this.creationDate = creationDate;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder lastUpdateDate(Instant lastUpdateDate) {
         ensureReconstitute();
         this.lastUpdateDate = lastUpdateDate;
         return this;
      }

      @Setter
      public DocumentMetadataBuilder attributes(Map<String, Object> metadata) {
         this.attributes = mutableMap(metadata);
         return this;
      }

      public DocumentMetadataBuilder addAttribute(String key, Object value) {
         this.attributes.put(key, value);
         return this;
      }

      public DocumentMetadata build() {
         return new DocumentMetadata(this);
      }
   }

}

