/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.domain;

import static com.tinubu.commons.ddd.invariant.Invariant.validate;
import static com.tinubu.commons.ddd.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd.invariant.rules.ComparableRules.isEqualTo;
import static com.tinubu.commons.ddd.invariant.rules.EqualsRules.isNotEqualTo;
import static com.tinubu.commons.ddd.invariant.rules.OptionalRules.isEmpty;
import static com.tinubu.commons.ddd.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.ports.document.domain.DocumentContent.SYSTEM_CONTENT_ENCODING;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.function.UnaryOperator;

import org.springframework.util.MimeType;

import com.tinubu.commons.ddd.domain.model.DomainBuilder;
import com.tinubu.commons.ddd.domain.type.Entity;
import com.tinubu.commons.ddd.domain.type.Fields;
import com.tinubu.commons.ddd.domain.type.support.LazyFields;
import com.tinubu.commons.ddd.invariant.InvariantRule;
import com.tinubu.commons.ddd.invariant.ParameterValue;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.LoadedDocumentContent.LoadedDocumentContentBuilder;
import com.tinubu.commons.ports.document.domain.StreamDocumentContent.StreamDocumentContentBuilder;

/**
 * Generic document representation entity with metadata and content.
 */
public class Document implements Entity<Document, DocumentId> {

   private final DocumentId documentId;
   private final DocumentMetadata metadata;
   private final DocumentContent content;

   private final LazyFields<Document> domainFields = new LazyFields<>(this::defineDomainFields);

   private Document(DocumentBuilder builder) {
      this.documentId = builder.documentId;
      this.content = builder.content;

      validateInvariants("dependency").orThrow();

      this.metadata = defaultMetadata(builder.metadata, content.contentEncoding());

      validateInvariants().orThrow();
   }

   public Fields<Document> defineDomainFields() {
      return fieldsBuilder()
            .field("documentId", v -> v.documentId, isNotNull(), "dependency")
            .field("metadata",
                   v -> v.metadata,
                   isNotNull().andValue(hasCorrectContentSize()).andValue(hasCorrectContentTypeCharset()))
            .field("content", v -> v.content, isNotNull(), "dependency")
            .build();
   }

   @Override
   public Fields<Document> domainFields() {
      return domainFields.get();
   }

   /**
    * Checks if metadata content size correctly matches effective document content size, if effective document
    * content size is available.
    */
   private InvariantRule<DocumentMetadata> hasCorrectContentSize() {
      ParameterValue<Long> effectiveContentSize =
            ParameterValue.lazyValue(() -> content.contentSize().orElse(null), "content.contentSize");

      return property(DocumentMetadata::contentSize,
                      "contentSize",
                      isEmpty().orValue(optionalValue(isEqualTo(effectiveContentSize).ifIsSatisfied(
                            effectiveContentSize::nonNull))));
   }

   /**
    * Checks if metadata content type charset correctly matches effective document content encoding.
    */
   private InvariantRule<DocumentMetadata> hasCorrectContentTypeCharset() {
      ParameterValue<Charset> contentEncoding =
            ParameterValue.lazyValue(() -> content.contentEncoding(), "content.contentEncoding");

      return property(documentMetadata -> documentMetadata.contentType().getCharset(),
                      "contentType.charset",
                      isEqualTo(contentEncoding));
   }

   @Override
   public DocumentId id() {
      return documentId();
   }

   @Getter
   public DocumentId documentId() {
      return documentId;
   }

   @Getter
   public DocumentMetadata metadata() {
      return metadata;
   }

   @Getter
   public DocumentContent content() {
      return content;
   }

   /**
    * Constructs a {@link DocumentEntry} from this document.
    *
    * @return a {@link DocumentEntry} of this document
    */
   public DocumentEntry documentEntry() {
      return new DocumentEntryBuilder().documentId(documentId).metadata(metadata).build();
   }

   /**
    * Updates document metadata.
    *
    * @param metadata document metadata. Specified metadata will be used as-is
    *       without any change
    *
    * @return new document
    */
   public Document metadata(DocumentMetadata metadata) {
      return DocumentBuilder.from(this).metadata(metadata).build();
   }

   /**
    * Updates document metadata for advanced usages.
    *
    * @param metadataOperator document metadata operator to use to update metadata. Specified metadata
    *       will be used as-is without any change
    *
    * @return new document
    */
   public Document metadata(UnaryOperator<DocumentMetadataBuilder> metadataOperator) {
      return metadata(DocumentMetadataBuilder.from(metadata).chain(metadataOperator).build());
   }

   /**
    * Updates document metadata document name.
    *
    * @param documentName document metadata path
    *
    * @return new document
    */
   public Document documentName(Path documentName) {
      return metadata(builder -> builder.documentName(documentName));
   }

   /**
    * Updates document metadata content type. Overriding content encoding using charset parameter is not
    * allowed and will result in an error if charset does not match actual content encoding.
    *
    * @param contentType document metadata content type
    *
    * @return new document
    */
   public Document contentType(MimeType contentType) {
      MimeType newContentType = contentType.getCharset() != null
                                ? contentType
                                : new MimeType(contentType, content.contentEncoding());

      return metadata(builder -> builder.contentType(newContentType));
   }

   /**
    * Updates document metadata attributes.
    *
    * @param attributes document metadata attributes
    *
    * @return new document
    */
   public Document attributes(Map<String, Object> attributes) {
      return metadata(builder -> builder.attributes(attributes));
   }

   /**
    * Updates document metadata attributes.
    *
    * @param attributes document metadata attributes
    *
    * @return new document
    */
   public Document addAttributes(Map<String, Object> attributes) {
      Map<String, Object> metadataAttributes = new HashMap<>(metadata.attributes());
      metadataAttributes.putAll(attributes);

      return attributes(metadataAttributes);
   }

   /**
    * Updates document metadata attributes.
    *
    * @param key document metadata attribute key
    * @param value document metadata attribute value
    *
    * @return new document
    */
   public Document addAttribute(String key, Object value) {
      Map<String, Object> metadataAttributes = new HashMap<>(metadata.attributes());
      metadataAttributes.put(key, value);

      return attributes(metadataAttributes);
   }

   /**
    * Updates document content, but not its identifier, name, content type. Metadata will be corrected
    * accordingly.
    *
    * @param content document content to set for this document
    *
    * @return new document
    */
   public Document content(DocumentContent content) {
      return DocumentBuilder
            .from(this)
            .content(content)
            .metadata(DocumentMetadataBuilder
                            .from(metadata)
                            .optionalChain(content.contentSize(), DocumentMetadataBuilder::contentSize)
                            .contentType(new MimeType(metadata.contentType(), content.contentEncoding()))
                            .build())
            .build();
   }

   /**
    * Duplicates this document and identifies new document with specified identifier. Document metadata
    * path is not updated in the process so both documents will have the same name, you can
    * update the metadata path afterward.
    *
    * @param newDocumentId new document identifier
    *
    * @return new duplicated document with new identifier
    */
   public Document duplicate(DocumentId newDocumentId) {
      validate(newDocumentId, "newDocumentId", isNotEqualTo(value(documentId, "documentId"))).orThrow();

      return DocumentBuilder.from(this).documentId(newDocumentId).build();
   }

   /**
    * Derives a default {@link DocumentMetadata} from available data.
    *
    * @param metadata building metadata
    * @param contentEncoding building content encoding
    *
    * @return document metadata
    *
    * @apiNote Document metadata document name use document identifier path by default.
    */
   private DocumentMetadata defaultMetadata(final DocumentMetadata metadata, final Charset contentEncoding) {
      DocumentMetadata defaultMetadata;

      if (metadata == null) {
         defaultMetadata = DocumentMetadataBuilder
               .newDocument()
               .documentName(this.documentId.value())
               .optionalChain(content.contentSize(), DocumentMetadataBuilder::contentSize)
               .contentType(contentEncoding)
               .build();
      } else {
         defaultMetadata = DocumentMetadataBuilder
               .from(metadata)
               .optionalChain(metadata.contentSize().or(content::contentSize),
                              DocumentMetadataBuilder::contentSize)
               .build();
      }

      return defaultMetadata;
   }

   @Override
   public boolean equals(Object o) {
      return entityEquals(o);
   }

   @Override
   public int hashCode() {
      return entityHashCode();
   }

   @Override
   public String toString() {
      return entityToString();
   }

   public static DocumentBuilder reconstituteBuilder() {
      return new DocumentBuilder().reconstitute();
   }

   public static class DocumentBuilder extends DomainBuilder<Document, DocumentBuilder> {
      private DocumentId documentId;
      private DocumentMetadata metadata;
      private DocumentContent content;

      /**
       * Copy builder constructor for {@link Document}
       *
       * @param document document to copy
       *
       * @return document builder
       */
      public static DocumentBuilder from(Document document) {
         return new DocumentBuilder()
               .documentId(document.documentId)
               .metadata(document.metadata)
               .content(document.content);
      }

      /**
       * Generates a document from local {@link File}.
       *
       * @param documentId generated document identifier
       * @param file file to generate this document from
       * @param fileEncoding file encoding
       *
       * @return document builder
       *
       * @throws IOException if an I/O error occurs while reading file
       */
      public static DocumentBuilder ofFile(DocumentId documentId, File file, Charset fileEncoding)
            throws IOException {
         BasicFileAttributes basicFileAttributes =
               Files.readAttributes(file.toPath(), BasicFileAttributes.class);
         Instant creationDate = basicFileAttributes.creationTime().toInstant();
         Instant lastUpdateDate = basicFileAttributes.lastModifiedTime().toInstant();

         return new DocumentBuilder()
               .documentId(documentId)
               .metadata(new DocumentMetadataBuilder()
                               .reconstitute(true)
                               .creationDate(creationDate)
                               .lastUpdateDate(lastUpdateDate)
                               .reconstitute(false)
                               .contentType(fileEncoding)
                               .contentSize(file.length())
                               .build())
               .streamContent(new FileInputStream(file), fileEncoding);
      }

      /**
       * Generates a document from local {@link File} using {@link DocumentContent#SYSTEM_CONTENT_ENCODING
       * system content encoding}.
       *
       * @param documentId generated document identifier
       * @param file file to generate this document from
       *
       * @return document builder
       *
       * @throws IOException if an I/O error occurs while reading file
       */
      public static DocumentBuilder ofFile(DocumentId documentId, File file) throws IOException {
         return ofFile(documentId, file, SYSTEM_CONTENT_ENCODING);
      }

      @Setter
      public DocumentBuilder documentId(DocumentId documentId) {
         this.documentId = documentId;
         return this;
      }

      @Setter
      public DocumentBuilder metadata(DocumentMetadata metadata) {
         this.metadata = metadata;
         return this;
      }

      public DocumentBuilder documentEntry(DocumentEntry documentEntry) {
         this.documentId = documentEntry.documentId();
         this.metadata = documentEntry.metadata();
         return this;
      }

      @Setter
      public DocumentBuilder content(DocumentContent content) {
         this.content = content;
         return this;
      }

      public DocumentBuilder loadedContent(byte[] content, Charset contentCharset) {
         return content(new LoadedDocumentContentBuilder().content(content, contentCharset).build());
      }

      public DocumentBuilder loadedContent(String content) {
         return content(new LoadedDocumentContentBuilder().content(content).build());
      }

      public DocumentBuilder loadedContent(InputStream content, Charset contentCharset) {
         return content(new LoadedDocumentContentBuilder().content(content, contentCharset).build());
      }

      public DocumentBuilder loadedContent(Reader content) {
         return content(new LoadedDocumentContentBuilder().content(content).build());
      }

      public DocumentBuilder streamContent(byte[] content, Charset contentCharset) {
         return content(new StreamDocumentContentBuilder().content(content, contentCharset).build());
      }

      public DocumentBuilder streamContent(String content) {
         return content(new StreamDocumentContentBuilder().content(content).build());
      }

      public DocumentBuilder streamContent(InputStream content, Charset contentCharset) {
         return content(new StreamDocumentContentBuilder().content(content, contentCharset).build());
      }

      public DocumentBuilder streamContent(Reader content) {
         return content(new StreamDocumentContentBuilder().content(content).build());
      }

      public Document build() {
         return new Document(this);
      }

   }

}

