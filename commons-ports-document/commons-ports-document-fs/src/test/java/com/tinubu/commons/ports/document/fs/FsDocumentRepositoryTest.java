/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import com.tinubu.commons.ports.document.domain.CommonDocumentRepositoryTest;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;

class FsDocumentRepositoryTest extends CommonDocumentRepositoryTest {

   private static Path TEST_STORAGE_PATH;

   private FsDocumentRepository documentRepository;

   @BeforeAll
   public static void createStoragePath() {
      try {
         TEST_STORAGE_PATH = Files.createTempDirectory("chassis-document-fs-test");

         System.out.printf("Creating %s as filesystem storage directory%n", TEST_STORAGE_PATH);
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   @AfterAll
   public static void deleteStoragePath() {
      try {
         System.out.printf("Deleting test storage path %s%n", TEST_STORAGE_PATH);

         FileUtils.deleteDirectory(TEST_STORAGE_PATH.toFile());
      } catch (IOException e) {
         throw new IllegalStateException(e);
      }
   }

   @BeforeEach
   public void configureDocumentRepository() {
      this.documentRepository = newFsDocumentRepository();
   }

   @Override
   public DocumentRepository documentRepository() {
      return documentRepository;
   }

   private FsDocumentRepository newFsDocumentRepository() {
      FsDocumentConfigPoint fsDocumentConfig = new FsDocumentConfigPoint();
      fsDocumentConfig.setStoragePath(TEST_STORAGE_PATH);
      fsDocumentConfig.setStorageStrategy(DirectFsStorageStrategy.class);

      return new FsDocumentRepository(fsDocumentConfig);
   }

}