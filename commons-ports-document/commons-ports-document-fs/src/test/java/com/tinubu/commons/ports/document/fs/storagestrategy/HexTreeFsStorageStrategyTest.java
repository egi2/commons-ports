/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs.storagestrategy;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.nio.file.Path;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ports.document.domain.DocumentId;
import com.tinubu.commons.ports.document.fs.FsDocumentConfigPoint;
import com.tinubu.commons.ports.document.fs.FsDocumentConfigPoint.HexTreeStorageStrategyConfig;

class HexTreeFsStorageStrategyTest {

   @Test
   public void testStorageFileWhenNominal() {
      assertThat(newStorageStrategy(2).storageFile(new DocumentId("path", "file.ext"))).isEqualTo(Path.of(
            "/tmp/test",
            "6a/82",
            "path/file.ext"));

      assertThat(HexTreeFsStorageStrategy.pathHexaString(Path.of("path", "file.ext"))).startsWith("6a82");
   }

   @Test
   public void testStorageFileWhenSimpleFile() {
      assertThat(newStorageStrategy(2).storageFile(new DocumentId("file.ext"))).isEqualTo(Path.of("/tmp/test",
                                                                                                  "a6/fd",
                                                                                                  "file.ext"));
   }

   @Test
   public void testStorageFileWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> newStorageStrategy(2).storageFile(null));
   }

   @Test
   public void testStorageFileWhenBadTreeDepth() {

      assertThatIllegalStateException()
            .isThrownBy(() -> newStorageStrategy(-1).storageFile(new DocumentId("path/file.ext")))
            .withMessage(
                  "Bad '-1' tree depth for 'path/file.ext' document hash : 6a82347ef62796aac6d221ccfc823dc4");

      assertThatIllegalStateException()
            .isThrownBy(() -> newStorageStrategy(0).storageFile(new DocumentId("path/file.ext")))
            .withMessage(
                  "Bad '0' tree depth for 'path/file.ext' document hash : 6a82347ef62796aac6d221ccfc823dc4");

      assertThat(newStorageStrategy(16).storageFile(new DocumentId("path", "file.ext"))).isEqualTo(Path.of(
            "/tmp/test",
            "6a/82/34/7e/f6/27/96/aa/c6/d2/21/cc/fc/82/3d/c4",
            "path/file.ext"));

      assertThatIllegalStateException()
            .isThrownBy(() -> newStorageStrategy(17).storageFile(new DocumentId("path", "file.ext")))
            .withMessage(
                  "Bad '17' tree depth for 'path/file.ext' document hash : 6a82347ef62796aac6d221ccfc823dc4");
   }

   private FsStorageStrategy newStorageStrategy(int treeDepth) {
      FsDocumentConfigPoint fsDocumentConfig = new FsDocumentConfigPoint();
      fsDocumentConfig.setStoragePath(Path.of("/tmp/test"));
      fsDocumentConfig.setStorageStrategy(HexTreeFsStorageStrategy.class);
      HexTreeStorageStrategyConfig hexTreeStorageStrategy = new HexTreeStorageStrategyConfig();
      hexTreeStorageStrategy.setTreeDepth(treeDepth);
      fsDocumentConfig.setHexTreeStorageStrategy(hexTreeStorageStrategy);

      return FsStorageStrategyFactory.fsStorageStrategy(fsDocumentConfig);
   }

   @Test
   public void testDocumentIdWhenNominal() {
      assertThat(newStorageStrategy(2).documentId(Path.of("/tmp/test", "6a/82", "path/file.ext"))).isEqualTo(
            new DocumentId("path/file.ext"));
   }

   @Test
   public void testDocumentIdWhenBadStoragePath() {
      assertThatIllegalStateException()
            .isThrownBy(() -> newStorageStrategy(2).documentId(Path.of("/badStoragePath",
                                                                       "6a/82",
                                                                       "path/file.ext")))
            .withMessage("Unsupported '/badStoragePath/6a/82/path/file.ext' storage file path");
   }

   @Test
   public void testDocumentIdWhenBadFileFormat() {
      assertThatIllegalStateException()
            .isThrownBy(() -> newStorageStrategy(2).documentId(Path.of("/tmp/test", "path/file.ext")))
            .withMessage("Unsupported '/tmp/test/path/file.ext' storage file path");
      assertThat(newStorageStrategy(2).documentId(Path.of("/tmp/test", "6a", "path/file.ext")))
            .as("the path name count must be greater that tree depth, the correctness of the path name is not checked, so 'path' is considered part of hexadecimal tree")
            .isEqualTo(new DocumentId("file.ext"));
      assertThatIllegalStateException()
            .isThrownBy(() -> newStorageStrategy(2).documentId(Path.of("/tmp/test", "6a", "file.ext")))
            .withMessage("Unsupported '/tmp/test/6a/file.ext' storage file path");
      assertThat(newStorageStrategy(2).documentId(Path.of("/tmp/test", "6a/82/83", "path/file.ext")))
            .as("the path name count must be greater that tree depth, the correctness of the path name is not checked, so '83' is considered part of document path")
            .isEqualTo(new DocumentId("83/path/file.ext"));
   }

}