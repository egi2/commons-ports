/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs.storagestrategy;

import java.lang.reflect.InvocationTargetException;

import com.tinubu.commons.ports.document.fs.FsDocumentConfigPoint;

/**
 * Generates a storage strategy.
 */
public class FsStorageStrategyFactory {

   public static FsStorageStrategy fsStorageStrategy(FsDocumentConfigPoint fsDocumentConfig) {
      return fsStorageStrategyInstance(fsDocumentConfig);
   }

   /**
    * Instantiate {@link FsStorageStrategy} from class.
    *
    * @return filesystem storage strategy instance
    */
   private static FsStorageStrategy fsStorageStrategyInstance(FsDocumentConfigPoint fsDocumentConfig) {
      try {
         return fsDocumentConfig.getStorageStrategy().getDeclaredConstructor(FsDocumentConfigPoint.class).newInstance(fsDocumentConfig);
      } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
         throw new IllegalStateException(e);
      }
   }

}
