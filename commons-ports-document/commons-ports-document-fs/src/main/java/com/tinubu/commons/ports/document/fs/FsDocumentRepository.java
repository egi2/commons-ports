/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.tinubu.commons.ddd.domain.specification.Specification;
import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentAccessException;
import com.tinubu.commons.ports.document.domain.DocumentContent;
import com.tinubu.commons.ports.document.domain.DocumentEntry;
import com.tinubu.commons.ports.document.domain.DocumentEntry.DocumentEntryBuilder;
import com.tinubu.commons.ports.document.domain.DocumentId;
import com.tinubu.commons.ports.document.domain.DocumentMetadata;
import com.tinubu.commons.ports.document.domain.DocumentMetadata.DocumentMetadataBuilder;
import com.tinubu.commons.ports.document.domain.DocumentRepository;
import com.tinubu.commons.ports.document.domain.FilteringDocumentEntrySpecification;
import com.tinubu.commons.ports.document.domain.StreamDocumentContent.StreamDocumentContentBuilder;
import com.tinubu.commons.ports.document.fs.storagestrategy.DirectFsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategy;
import com.tinubu.commons.ports.document.fs.storagestrategy.FsStorageStrategyFactory;

@Repository("commons-ports.document.fs.FsDocumentRepository")
public class FsDocumentRepository implements DocumentRepository {
   private static final Logger log = LoggerFactory.getLogger(FsDocumentRepository.class);

   private static final StopWatch watch = new StopWatch();

   private final FsStorageStrategy fsStorageStrategy;
   private final Charset documentEncoding;
   private final Path storagePath;

   public FsDocumentRepository(FsDocumentConfigPoint fsDocumentConfig) {
      this.fsStorageStrategy = FsStorageStrategyFactory.fsStorageStrategy(fsDocumentConfig);
      this.documentEncoding = fsDocumentConfig.getDocumentEncoding();
      this.storagePath = fsDocumentConfig.getStoragePath().toAbsolutePath();
   }

   @Override
   public Optional<Document> findDocumentById(DocumentId documentId) {
      File documentFile = storageFile(documentId);

      return listDocumentEntry(documentFile, documentId).flatMap(entry -> {
         try {
            return Optional.of(new DocumentBuilder()
                                     .reconstitute()
                                     .documentEntry(entry)
                                     .content(fsDocumentContent(documentFile))
                                     .build());
         } catch (FileNotFoundException e) {
            return Optional.empty();
         }
      });
   }

   @Override
   public Stream<Document> findDocumentsBySpecification(Specification<DocumentEntry> specification) {
      return findDocumentEntriesBySpecification(specification).flatMap(entry -> {
         try {
            return Stream.of(new DocumentBuilder()
                                   .reconstitute()
                                   .documentEntry(entry)
                                   .content(fsDocumentContent(storageFile(entry.documentId())))
                                   .build());
         } catch (FileNotFoundException e) {
            return Stream.empty();
         }
      });
   }

   @Override
   public Optional<DocumentEntry> findDocumentEntryById(DocumentId documentId) {
      File documentFile = storageFile(documentId);

      return listDocumentEntry(documentFile, documentId);
   }

   /**
    * {@inheritDoc}
    *
    * @implNote Optimizes listing if specification is a {@link FilteringDocumentEntrySpecification} and
    *       filesystem storage strategy is {@link DirectFsStorageStrategy}. In the other cases, all documents
    *       are listed and filtered which is not performant.
    */
   @Override
   public Stream<DocumentEntry> findDocumentEntriesBySpecification(Specification<DocumentEntry> specification) {
      Stream<DocumentEntry> documentEntries;

      if (specification instanceof FilteringDocumentEntrySpecification
          && fsStorageStrategy instanceof DirectFsStorageStrategy) {
         FilteringDocumentEntrySpecification documentEntrySpecification =
               (FilteringDocumentEntrySpecification) specification;

         if (documentEntrySpecification.directories().isEmpty()) {
            documentEntries = listAllDocumentEntries(documentEntrySpecification.extensions());
         } else {
            documentEntries = documentEntrySpecification
                  .directories()
                  .stream()
                  .flatMap(directory -> listDocumentEntries(directory,
                                                            documentEntrySpecification.extensions(),
                                                            false));
         }
      } else {
         documentEntries = listAllDocumentEntries(null);
      }

      return documentEntries.filter(specification::satisfiedBy);
   }

   @Override
   public boolean saveDocument(Document document, boolean overwrite) {
      File documentFile = storageFile(document.documentId());

      if (!overwrite && documentFile.exists()) {
         return false;
      }

      watch.reset();
      watch.start();

      mkdirStorageFile(documentFile);

      try (InputStream documentInputStream = document.content().inputStreamContent();
           OutputStream documentOutputStream = new FileOutputStream(documentFile)) {
         documentInputStream.transferTo(documentOutputStream);
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }

      watch.stop();

      log.info("Document '{}' -> '{}' successfully saved in {}ms",
               document.documentId().idString(),
               documentFile,
               watch.getTime(TimeUnit.MILLISECONDS));

      return true;
   }

   @Override
   public boolean deleteDocument(DocumentId documentId) {
      watch.reset();
      watch.start();

      File documentFile = storageFile(documentId);
      if (!documentFile.delete()) {
         return false;
      }

      watch.stop();

      log.info("Document '{}' -> '{}' successfully deleted in {}ms",
               documentId.idString(),
               documentFile,
               watch.getTime(TimeUnit.MILLISECONDS));

      return true;
   }

   /**
    * List specified document entry.
    *
    * @param storageFile storage file
    * @param documentId document identifier, when known, to optimize reverse document id mapping
    */
   private Optional<DocumentEntry> listDocumentEntry(File storageFile, DocumentId documentId) {
      notNull(storageFile, "storageFile");
      notNull(documentId, "documentId");

      if (storageFile.exists()) {
         return Optional.of(fsDocumentEntry(storageFile));
      } else {
         return Optional.empty();
      }
   }

   /**
    * List specified document entry.
    *
    * @param storageFile storage file
    */
   private Optional<DocumentEntry> listDocumentEntry(File storageFile) {
      return listDocumentEntry(storageFile, documentId(storageFile));
   }

   /**
    * List recursively all document entries.
    *
    * @param extensions optional list of file extensions to search (without '.')
    */
   private Stream<DocumentEntry> listAllDocumentEntries(List<String> extensions) {
      return listDocumentEntries(storagePath, extensions, true);
   }

   /**
    * List document entries from specified base directory. Base directory represents a physical path on the
    * filesystem. It can be relative or absolute, empty or not.
    *
    * @param baseDirectory base directory to search from
    * @param extensions optional list of file extensions to search (without '.'). No extension filtering
    *       applied if list is {@code null} or empty.
    * @param recursive search recursively into sub-directories
    *
    * @apiNote Specified base directory is directly resolved against configured storage path without
    *       taking {@link FsStorageStrategy strategies} into account. This operation should not be used with
    *       {@link FsStorageStrategy strategies} other than {@link DirectFsStorageStrategy}.
    */
   private Stream<DocumentEntry> listDocumentEntries(Path baseDirectory,
                                                     List<String> extensions,
                                                     boolean recursive) {
      notNull(baseDirectory, "baseDirectory");

      Path searchDirectory = storagePath.resolve(baseDirectory);

      String[] extensionsFilter =
            extensions == null || extensions.isEmpty() ? null : extensions.toArray(new String[0]);

      return StreamSupport
            .stream(Spliterators.spliteratorUnknownSize(FileUtils.iterateFiles(searchDirectory.toFile(),
                                                                               extensionsFilter,
                                                                               recursive),
                                                        Spliterator.ORDERED), false)
            .filter(this::isRegularFile)
            .map(this::fsDocumentEntry);
   }

   private boolean isRegularFile(File file) {
      try {
         return Files.readAttributes(file.toPath(), BasicFileAttributes.class).isRegularFile();
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   private DocumentEntry fsDocumentEntry(File storageFile) {
      return new DocumentEntryBuilder()
            .reconstitute()
            .documentId(documentId(storageFile))
            .metadata(fsDocumentMetadata(storageFile))
            .build();
   }

   private DocumentContent fsDocumentContent(File storageFile) throws FileNotFoundException {
      return new StreamDocumentContentBuilder()
            .content(new FileInputStream(storageFile), documentEncoding)
            .build();
   }

   private DocumentMetadata fsDocumentMetadata(File storageFile) {
      try {
         BasicFileAttributes basicFileAttributes =
               Files.readAttributes(storageFile.toPath(), BasicFileAttributes.class);
         Instant creationDate = basicFileAttributes.creationTime().toInstant();
         Instant lastUpdateDate = basicFileAttributes.lastModifiedTime().toInstant();

         return new DocumentMetadataBuilder()
               .reconstitute()
               .documentName(documentId(storageFile).value())
               .contentSize(storageFile.length())
               .contentType(documentEncoding)
               .creationDate(creationDate)
               .lastUpdateDate(lastUpdateDate)
               .build();
      } catch (IOException e) {
         throw new DocumentAccessException(e);
      }
   }

   /**
    * Creates parent directories of specified storageFile. Does nothing if storageFile has no parent
    * directory.
    *
    * @param storageFile storageFile
    */
   private void mkdirStorageFile(File storageFile) {
      notNull(storageFile, "storageFile");

      File directory = storageFile.getParentFile();
      if (directory != null) {
         if (!(directory.mkdirs() || directory.isDirectory())) {
            throw new DocumentAccessException(String.format("Can't create '%s' filesystem storage directory",
                                                            directory));
         }
      }
   }

   /**
    * Generate real local storage path for specified document.
    *
    * @param documentId document id
    *
    * @return real document local storage path
    *
    * @apiNote The returned file is protected against path traversal attacks.
    */
   private File storageFile(DocumentId documentId) {
      return fsStorageStrategy.storageFile(documentId).toFile();
   }

   /**
    * Returns document identifier from storage document file. Reverses path creation logic to generate a
    * logical document path.
    *
    * @param storageFile storage document file
    *
    * @return document identifier from storage file
    */
   private DocumentId documentId(File storageFile) {
      return fsStorageStrategy.documentId(storageFile.toPath());
   }
}
