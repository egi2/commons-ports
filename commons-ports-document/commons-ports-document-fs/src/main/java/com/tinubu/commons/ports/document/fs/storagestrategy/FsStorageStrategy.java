/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs.storagestrategy;

import java.nio.file.Path;

import com.tinubu.commons.ports.document.domain.DocumentId;

/**
 * Strategy to store a document into the filesystem.
 * <p>
 * Generated storage file path must be absolute, and deterministic for a given document identifier.
 */
public interface FsStorageStrategy {

   Path storageFile(DocumentId documentId);

   /**
    * Returns document identifier from storage document file. Reverses path creation logic to generate a
    * logical document identifier.
    *
    * @param storageFile storage document file
    *
    * @return document identifier from storage file
    */
   DocumentId documentId(Path storageFile);

}
