/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.document.fs.autoconfigure;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.tinubu.commons.ports.document.fs.FsDocumentConfigPoint;
import com.tinubu.commons.ports.document.fs.FsDocumentRepository;

/**
 * Filesystem document adapter auto-configuration.
 */
@Configuration
@Import({ FsDocumentRepository.class })
@EnableConfigurationProperties({ FsDocumentConfigPoint.class })
public class CommonPortsDocumentFsAutoConfiguration {

   private static final Logger log = LoggerFactory.getLogger(CommonPortsDocumentFsAutoConfiguration.class);

   @Autowired
   FsDocumentConfigPoint fsDocumentConfig;

   @PostConstruct
   public void logFsDocumentConfiguration() {
      log.info("Using filesystem document configuration {}", fsDocumentConfig);
   }

}
