#!/usr/bin/env bash

#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

set -e

test -z "$AWK" && AWK="awk"
test -z "$MVN" && MVN="mvn"
test -z "$JAR" && { test -z "$JAVA_HOME" && JAR="jar" || JAR="$JAVA_HOME/bin/jar"; }

date-iso8601() {
  date -Iseconds
}

color() {
  color="$1"
  shift 1

  echo -e "\033[${color}m$@\033[m"
}

prefix() {
  prefix="$1"
  seds '^' "$prefix" 'g'
}

output() {
  set -o pipefail
  "$@" 2>&1 | prefix "$(color 34 "|") "
  set +o pipefail
}

warning() {
  echo "$@" | prefix "[$(color '33;1' "W")] "
}

error() {
  echo "$@" | prefix "[$(color '31;1' "E")] "
}

fatal-error() {
  errCode="$1"
  shift 1
  echo "$@" | prefix "[$(color '31;1' "E")] "
  exit $errCode
}

title() {
  echo "$@" | prefix "$(color '32;1' "--") "
}

info() {
  echo "$@"
}

# Multi-platform realpath command.
realpath_cmd() {
  case "$(uname -s)" in
  "Darwin") realpath "$@" ;;
  *)
    if which realpath &>/dev/null; then
      realpath "$@"
    else
      readlink -f "$@"
    fi
    ;;
  esac
}

# Echoes specified command on stdout.
setx() {
  set -x
  "$@"
  {
    errCode=$?
    set +x
  } 2>/dev/null
  return ${errCode}
}

# Sed append once
seda1() {
  sed "/$1/{s/.*/&\n$2/;:a;n;ba}"
}

# Sed append
seda() {
  sed "/$1/{s/.*/&\n$2/}"
}

# Sed insert once
sedi1() {
  sed "/$1/{s/.*/$2\n&/;:a;n;ba}"
}

# Sed insert
sedi() {
  sed "/$1/{s/.*/$2\n&/}"
}

# Sed replace
seds() {
  sed "s/$1/$2/$3"
}

usage() {
  message="$1"
  errCode="$2"

  error "$message"
  echo "Usage : $(basename "$0") <command> [<arguments...>]"
  echo
  echo "  Commands :"
  echo "  test-failure-check                            Check surefire/failsafe reports for errors"
  echo "  test-jacoco-coverage                          Display JaCoCo coverage"
  echo "  maven-evaluate <expression> [<arguments...>]  Evaluate expression in Maven"
  echo "  jar-cat <jar-file> <entry>                    Extract entry from jar-file and dump it to stdout"
  echo "  jar-filter-instance <jar-file> <instance>     Recreate jar-file with filtered dependencies that are not restricted to instance"
  echo

  exit $errCode
}

# Analyzes the test-suites results and fails (return 1) is any test error occurred.
check-tests() {
  testReports="$(find . -path '*/*-reports/*' -name 'TEST-*.xml')"

  if ! test -z "$testReports"; then
    eval $($AWK 'BEGIN { tests = errors = skipped = failures = 0 } { match($0, /<testsuite.*tests="([0-9]+)".*errors="([0-9]+)".*skipped="([0-9]+)".*failures="([0-9]+)".*>/, groups); tests += groups[1]; errors += groups[2]; skipped += groups[3]; failures += groups[4] } END { print "TEST_COUNT=" tests ";", "TEST_ERRORS=" errors ";", "TEST_SKIPPED=" skipped ";", "TEST_FAILURES=" failures ";", "TEST_ALL_ERRORS=" (errors + failures) }' $testReports)
    echo "Test results : $(color 34 count)=$(color 33 ${TEST_COUNT}), $(color 34 skipped)=$(color 33 ${TEST_SKIPPED}), $(color 34 errors)=$(color 33 ${TEST_ERRORS}), $(color 34 failures)=$(color 33 ${TEST_FAILURES})"
    if test $TEST_ALL_ERRORS -gt 0; then
      error "Test errors (${TEST_ERRORS}) + failures (${TEST_FAILURES}) ":" $TEST_ALL_ERRORS"
      return 1
    fi
  else
    warning "No test reports found"
  fi
}

# Reports global Jacoco coverage on stdout.
report-jacoco-coverage() {
  jacocoReports="$(find . -path "*/target/site/jacoco-[ui]t/*" -name 'jacoco.csv')"
  if ! test -z "$jacocoReports"; then
    $AWK -F"," '{ instructions += $4 + $5; covered += $5 } END { print "JaCoCo instructions coverage=" 100 * covered / instructions "% (" covered "/" instructions ")" }' $jacocoReports
  else
    warning "No JaCoCo reports found"
  fi
}

# Evaluates a Maven expression.
maven-evaluate() {
  expression="$1"
  shift 1

  $MVN help:evaluate -Dexpression="$expression" -q -DforceStdout -pl . "$@"
}

source-aliases() {
  functions=$(declare -F | $AWK '{print $3}')
  echo 'shopt -s expand_aliases;'
  for f in $functions; do
    echo 'alias gitlab-'$f"='"$0 $f"';"
  done
}

# In-depth variables evaluation.
# Usage :
# $> export VAR1 VAR2 VAR3
# $> eval $(a_var="a_value" sh-eval VAR1 VAR2 VAR3)
# or :
# $> eval $(a_var="a_value" VAR1="$VAR1" VAR2="$VAR2" VAR3="$VAR3" sh-eval VAR1 VAR2 VAR3)
sh-eval() {
  for var_name in "$@"; do
    eval $var_name='$'"$var_name"
    eval var_value='$'"$var_name"
    # 3-depth recursive evaluation :
    eval var_value=\"$var_value\"
    eval var_value=\"$var_value\"
    eval var_value=\"$var_value\"
    echo 'export '$var_name'='"'$var_value'"
  done
}

# Extracts an entry file from a jar and dump the entry content to stdout.
# Returns with 1 if entry file does not exist in jar.
jar-cat () {
  test $# -lt 2 && usage "Bad parameters" 1
  jar="$1"
  entry="$2"
  shift 2

  TMP="$(mktemp -d)"
  jar="$(realpath_cmd "$jar")"
  (
     cd "$TMP"
     if $JAR xvf "$jar" "$entry" &> /dev/null; then
       test -f "$TMP/$entry" && cat "$TMP/$entry" || exit 1
     fi
  )
  ret=$?
  rm -rf "$TMP"
  return $ret
}

# Filters specified jar and inner jars into it for restricted instances.
# If jar/inner jar contains a META-INF/MANIFEST.MF with 'Restrict-Instances' metadata (space separated list of instances),
# it must list the specified instance. Otherwise, the jar is deleted (or inner jar into it).
jar-filter-instance () {
   test $# -lt 2 && usage "Bad parameters" 1
   jar="$1"
   instance="$2"
   shift 2

   echo "Filter '$jar' restricted to '$instance'"

   if restrictedInstances=$(filter-jar "$jar" "$instance"); then
     echo "  Remove $jar [$restrictedInstances]"
     rm -f "$jar"
   else
      TMP="$(mktemp -d)"
      jar="$(realpath_cmd "$jar")"
      (
       cd "$TMP"
       $JAR xf "$jar"
       updatedJar=false
       for packageJar in $(find . -name '*.jar'); do
          if restrictedInstances=$(filter-jar "$packageJar" "$instance"); then
             echo "  Remove $packageJar [$restrictedInstances]"
             rm "$packageJar"
             updatedJar=true
          fi
       done
       if $updatedJar; then
          cp -f "$jar" "$jar.beforefilter"
          $JAR --create -0 --file "$jar" --manifest META-INF/MANIFEST.MF .
       fi
      )
      rm -rf "$TMP"
   fi
}

# Tests if a JAR must be filtered by checking its MANIFEST.MF.
# Returns 0 if it must be filtered out, otherwise return 1. Also echoes "Restrict-Instances" metadata on stdout.
filter-jar () {
   test $# -lt 2 && usage "Bad parameters" 1
   jar="$1"
   instance="$2"
   shift 2

   if mf="$(jar-cat "$jar" META-INF/MANIFEST.MF | normalize-mf)"; then
      if restrictedInstances=$(echo "$mf" | grep -i 'Restrict-Instances: '); then
         for restrictedInstance in $(echo "$restrictedInstances" | sed 's/Restrict-Instances: //i'); do
            if test "$restrictedInstance" = "$instance"; then
               return 1
            fi
         done
         echo "$restrictedInstances"
         return 0
      fi
   fi

   return 1
}

# Un-split MF multi-lines and remove DOS carriage returns for stdin.
normalize-mf () {
  sed -e ':a' -e N -e '$!ba' -e 's/\r\n //g' | tr -d '\r'
}

main() {
  if test $# -gt 0; then
    command="$1"
    shift 1
  else
    usage "No command defined" 1
  fi

  $command "$@"
}

main "$@"
