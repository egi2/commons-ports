/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.smtp;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Repository;
import org.springframework.util.MimeType;

import com.tinubu.commons.ports.document.domain.Document;
import com.tinubu.commons.ports.message.domain.Message;
import com.tinubu.commons.ports.message.domain.MessageAddress;
import com.tinubu.commons.ports.message.domain.MessageDocument;
import com.tinubu.commons.ports.message.domain.MessageIOException;
import com.tinubu.commons.ports.message.domain.MessageService;

@Repository("commons-ports.message.smtp.SmtpMessageService")
public class SmtpMessageService implements MessageService {
   private static final Logger log = LoggerFactory.getLogger(SmtpMessageService.class);

   /** Multipart mode to use for client compatibility. */
   private static final int MULTIPART_MODE = MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED;

   private static final StopWatch watch = new StopWatch();

   private final JavaMailSender javaMailSender;

   public SmtpMessageService(JavaMailSender javaMailSender) {
      this.javaMailSender = javaMailSender;
   }

   @Override
   public void sendMessage(Message message) {
      watch.reset();
      watch.start();

      MimeMessage mimeMessage = javaMailSender.createMimeMessage();

      try {
         MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, MULTIPART_MODE);
         helper.setFrom(message.from().address());
         helper.setTo(message.to().address());
         helper.setCc(message.cc().stream().map(MessageAddress::address).toArray(String[]::new));
         helper.setBcc(message.bcc().stream().map(MessageAddress::address).toArray(String[]::new));
         if (message.subject().isPresent()) {
            helper.setSubject(message.subject().get());
         }
         helper.setText(message.content().content(), message.content().isHtml());

         for (MessageDocument attachment : message.attachments()) {
            helper.addAttachment(attachment.document().metadata().documentFileName(),
                                 documentDataSource(attachment.document()));
         }

         for (MessageDocument inline : message.inlines()) {
            helper.addInline(inline.messageDocumentId(), documentDataSource(inline.document()));
         }

         javaMailSender.send(mimeMessage);

         watch.stop();

         log.info("Message '{}' from {} to {} successfully sent in {}ms",
                  message.subject(),
                  message.from(),
                  message.to(),
                  watch.getTime(TimeUnit.MILLISECONDS));
      } catch (MessagingException | MailException | IOException e) {
         throw new MessageIOException(e.getMessage(), e);
      }
   }

   /**
    * Builds {@link DataSource} from {@link Document}. Ensures data source has correct content encoding from
    * specified document.
    *
    * @param document document to builds data source from
    *
    * @throws IOException if an I/O error occurs
    */
   private DataSource documentDataSource(Document document) throws IOException {
      return new ByteArrayDataSource(document.content().inputStreamContent(), documentContentType(document));
   }

   /**
    * Builds document content type MIME as a String including the mandatory charset from document encoding.
    *
    * @param document document to extract content type
    *
    * @return document content type including the charset
    */
   private String documentContentType(Document document) {
      MimeType contentType = document.metadata().contentType();

      if (contentType.getCharset() == null) {
         contentType = new MimeType(contentType, document.content().contentEncoding());
      }

      return contentType.toString();
   }

}
