package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.ddd.invariant.rules.StringRules.isNotBlank;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;

import org.springframework.util.MimeType;

import com.tinubu.commons.ddd.domain.model.DomainBuilder;
import com.tinubu.commons.ddd.domain.type.Fields;
import com.tinubu.commons.ddd.domain.type.Value;
import com.tinubu.commons.ddd.domain.type.support.LazyFields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

/**
 * Message content
 */
public class MessageContent implements Value<MessageContent> {

   /** Content encoding. */
   private static final Charset CONTENT_ENCODING = StandardCharsets.UTF_8;
   /** Recognized MIME types for HTML content. */
   private static final MimeType[] HTML_MIME_TYPES =
         { MimeType.valueOf("text/html"), MimeType.valueOf("application/xhtml+xml") };

   private final LazyFields<MessageContent> domainFields = new LazyFields<>(this::defineDomainFields);

   private final String content;
   private final MimeType contentType;

   private MessageContent(MessageContentBuilder builder) {
      this.content = builder.content;
      this.contentType = builder.contentType;

      validateInvariants().orThrow();
   }

   public Fields<MessageContent> defineDomainFields() {
      return fieldsBuilder()
            .field("content", v -> v.content, isNotBlank())
            .field("contentType",
                   v -> v.contentType,
                   isNotNull().andValue(property(MimeType::getCharset,
                                                 "charset",
                                                 isNull().orValue(isEqualTo(value(CONTENT_ENCODING))))))
            .build();
   }

   /**
    * Returns {@code true} if content is HTML.
    *
    * @return whether content is HTML
    */
   public boolean isHtml() {
      return Stream.of(HTML_MIME_TYPES).anyMatch(contentType::equalsTypeAndSubtype);
   }

   /**
    * Message content.
    *
    * @return message content
    */
   @Getter
   public String content() {
      return content;
   }

   /**
    * Message content type.
    *
    * @return message content type
    */
   @Getter
   public MimeType contentType() {
      return contentType;
   }

   @Override
   public boolean equals(Object o) {
      return valueEquals(o);
   }

   @Override
   public int hashCode() {
      return valueHashCode();
   }

   @Override
   public String toString() {
      return valueToString();
   }

   @Override
   public Fields<MessageContent> domainFields() {
      return domainFields.get();
   }

   public static MessageContentBuilder reconstituteBuilder() {
      return new MessageContentBuilder().reconstitute(true);
   }

   public static class MessageContentBuilder extends DomainBuilder<MessageContent, MessageContentBuilder> {
      private String content;
      private MimeType contentType;

      public static MessageContent create(String content, MimeType contentType) {
         return new MessageContentBuilder().content(content).contentType(contentType).build();
      }

      @Setter
      public MessageContentBuilder content(String content) {
         this.content = content;

         return this;
      }

      @Setter
      public MessageContentBuilder contentType(MimeType contentType) {
         this.contentType = contentType;

         return this;
      }

      @Override
      public MessageContent build() {
         return new MessageContent(this);
      }
   }

}
