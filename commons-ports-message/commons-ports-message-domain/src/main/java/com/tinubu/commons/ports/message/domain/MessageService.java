package com.tinubu.commons.ports.message.domain;

/**
 * Message communication service.
 */
public interface MessageService {

   /**
    * Sends a message.
    *
    * @param message message to send
    *
    * @throws MessageIOException if an error occurs while sending message
    */
   void sendMessage(Message message);

}
