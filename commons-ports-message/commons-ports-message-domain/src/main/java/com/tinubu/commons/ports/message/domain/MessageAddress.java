package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd.invariant.rules.StringRules.isNotBlank;

import com.tinubu.commons.ddd.domain.type.Fields;
import com.tinubu.commons.ddd.domain.type.Value;
import com.tinubu.commons.ddd.domain.type.support.LazyFields;
import com.tinubu.commons.lang.beans.Getter;

/**
 * Generic message address.
 */
public class MessageAddress implements Value<MessageAddress> {

   private final LazyFields<MessageAddress> domainFields = new LazyFields<>(this::defineDomainFields);

   private final String address;

   public MessageAddress(String address) {
      this.address = address;

      validateInvariants().orThrow();
   }

   public Fields<MessageAddress> defineDomainFields() {
      return fieldsBuilder().field("address", v -> v.address, isNotBlank()).build();
   }

   @Getter
   public String address() {
      return address;
   }

   @Override
   public boolean equals(Object o) {
      return valueEquals(o);
   }

   @Override
   public int hashCode() {
      return valueHashCode();
   }

   @Override
   public String toString() {
      return valueToString();
   }

   @Override
   public Fields<MessageAddress> domainFields() {
      return domainFields.get();
   }
}
