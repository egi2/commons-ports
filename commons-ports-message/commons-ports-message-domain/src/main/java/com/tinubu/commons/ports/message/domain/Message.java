package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd.invariant.rules.CollectionRules.hasNoDuplicates;
import static com.tinubu.commons.lang.model.ImmutableUtils.immutableList;
import static com.tinubu.commons.lang.model.MutableUtils.mutableList;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import com.tinubu.commons.ddd.domain.model.DomainBuilder;
import com.tinubu.commons.ddd.domain.type.Fields;
import com.tinubu.commons.ddd.domain.type.Value;
import com.tinubu.commons.ddd.domain.type.support.LazyFields;
import com.tinubu.commons.ddd.invariant.Invariant;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;

/**
 * Generic message representation.
 */
public class Message implements Value<Message> {

   private final LazyFields<Message> domainFields = new LazyFields<>(this::defineDomainFields);

   private final MessageAddress from;
   private final MessageAddress to;
   private final List<MessageAddress> cc;
   private final List<MessageAddress> bcc;
   private final String subject;
   private final MessageContent content;
   private final List<MessageDocument> attachments;
   private final List<MessageDocument> inlines;

   private Message(MessageBuilder builder) {
      this.from = builder.from;
      this.to = builder.to;
      this.cc = immutableList(builder.cc);
      this.bcc = immutableList(builder.bcc);
      this.subject = builder.subject;
      this.content = builder.content;
      this.attachments = immutableList(builder.attachments);
      this.inlines = immutableList(builder.inlines);

      validateInvariants().orThrow();
   }

   public Fields<Message> defineDomainFields() {
      return fieldsBuilder()
            .field("from", v -> v.from, isNotNull())
            .field("to", v -> v.to, isNotNull())
            .field("cc", v -> v.cc, isNotNull())
            .field("bcc", v -> v.bcc, isNotNull())
            .field("subject", v -> v.subject)
            .field("attachments", v -> v.attachments, v -> displayDocuments(v.attachments), isNotNull())
            .field("inlines", v -> v.inlines, v -> displayDocuments(v.inlines), isNotNull())
            .invariants(Invariant.of(() -> this,
                                     as(message -> Stream
                                              .concat(message.attachments().stream(), message.inlines().stream())
                                              .collect(toList()),
                                        hasNoDuplicates(MessageDocument::messageDocumentId))))
            .build();
   }

   private Object displayDocuments(List<MessageDocument> documents) {
      return documents
            .stream()
            .map(d -> d.messageDocumentId() + "->" + d.document().documentId())
            .collect(joining(",", "[", "]"));
   }

   /**
    * Sender of the message.
    *
    * @return message sender
    */
   @Getter
   public MessageAddress from() {
      return from;
   }

   /**
    * Receiver of the message.
    *
    * @return message receiver
    */
   @Getter
   public MessageAddress to() {
      return to;
   }

   /**
    * Optional Carbon Copy (CC) receivers of the message.
    *
    * @return message CC receivers or empty list
    */
   @Getter
   public List<MessageAddress> cc() {
      return cc;
   }

   /**
    * Optional Blind Carbon Copy (BCC) receivers of the message.
    *
    * @return message BCC receivers or empty list
    */
   @Getter
   public List<MessageAddress> bcc() {
      return bcc;
   }

   /**
    * Optional message subject.
    * Message subject can also be blank.
    *
    * @return message subject
    */
   @Getter
   public Optional<String> subject() {
      return nullable(subject);
   }

   /**
    * Message content.
    *
    * @return message content
    */
   @Getter
   public MessageContent content() {
      return content;
   }

   /**
    * Optional document attachments.
    *
    * @return attachments or empty list
    */
   @Getter
   public List<MessageDocument> attachments() {
      return attachments;
   }

   /**
    * Optional document inlines. These documents are referenced by {@link MessageDocument#messageDocumentId()}
    * in the content.
    *
    * @return attachments or empty list
    */
   @Getter
   public List<MessageDocument> inlines() {
      return inlines;
   }

   @Override
   public boolean equals(Object o) {
      return valueEquals(o);
   }

   @Override
   public int hashCode() {
      return valueHashCode();
   }

   @Override
   public String toString() {
      return valueToString();
   }

   @Override
   public Fields<Message> domainFields() {
      return domainFields.get();
   }

   public static MessageBuilder reconstituteBuilder() {
      return new MessageBuilder().reconstitute(true);
   }

   public static class MessageBuilder extends DomainBuilder<Message, MessageBuilder> {
      private MessageAddress from;
      private MessageAddress to;
      private List<MessageAddress> cc = new ArrayList<>();
      private List<MessageAddress> bcc = new ArrayList<>();
      private String subject;
      private MessageContent content;
      private List<MessageDocument> attachments = new ArrayList<>();
      private List<MessageDocument> inlines = new ArrayList<>();

      @Setter
      public MessageBuilder from(MessageAddress from) {
         this.from = from;

         return this;
      }

      @Setter
      public MessageBuilder to(MessageAddress to) {
         this.to = to;

         return this;
      }

      @Setter
      public MessageBuilder cc(List<MessageAddress> cc) {
         this.cc = mutableList(cc);

         return this;
      }

      public MessageBuilder addCc(MessageAddress... cc) {
         this.cc.addAll(immutableList(cc));

         return this;
      }

      @Setter
      public MessageBuilder bcc(List<MessageAddress> bcc) {
         this.bcc = mutableList(bcc);

         return this;
      }

      public MessageBuilder addBcc(MessageAddress... bcc) {
         this.bcc.addAll(immutableList(bcc));

         return this;
      }

      @Setter
      public MessageBuilder content(MessageContent content) {
         this.content = content;

         return this;
      }

      @Setter
      public MessageBuilder subject(String subject) {
         this.subject = subject;

         return this;
      }

      @Setter
      public MessageBuilder attachments(List<MessageDocument> attachments) {
         this.attachments = mutableList(attachments);

         return this;
      }

      public MessageBuilder addAttachments(MessageDocument... attachments) {
         this.attachments.addAll(immutableList(attachments));

         return this;
      }

      @Setter
      public MessageBuilder inlines(List<MessageDocument> inlines) {
         this.inlines = mutableList(inlines);

         return this;
      }

      public MessageBuilder addInlines(MessageDocument... inlines) {
         this.inlines.addAll(immutableList(inlines));

         return this;
      }

      @Override
      public Message build() {
         return new Message(this);
      }
   }

}
