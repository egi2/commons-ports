package com.tinubu.commons.ports.message.domain;

import static com.tinubu.commons.ddd.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import com.tinubu.commons.ddd.domain.model.DomainBuilder;
import com.tinubu.commons.ddd.domain.type.Fields;
import com.tinubu.commons.ddd.domain.type.Value;
import com.tinubu.commons.ddd.domain.type.support.LazyFields;
import com.tinubu.commons.lang.beans.Getter;
import com.tinubu.commons.lang.beans.Setter;
import com.tinubu.commons.ports.document.domain.Document;

/**
 * A document to attach to {@link Message}.
 * This representation adds support for a unique, normalized, document identifier in the scope of the message,
 * that can be used to inline a document by referencing it.
 */
public class MessageDocument implements Value<MessageDocument> {

   private final LazyFields<MessageDocument> domainFields = new LazyFields<>(this::defineDomainFields);

   private final Document document;
   private final String messageDocumentId;

   private MessageDocument(MessageDocumentBuilder builder) {
      this.document = builder.document;

      validateInvariants("required").orThrow();

      this.messageDocumentId = nullable(builder.messageDocumentId,
                                        this::normalizeMessageDocumentId,
                                        defaultMessageDocumentId(document));

      validateInvariants().orThrow();
   }

   public Fields<MessageDocument> defineDomainFields() {
      return fieldsBuilder()
            .field("document", v -> v.document, isNotNull(), "required")
            .field("messageDocumentId", v -> v.messageDocumentId, isNotBlank())
            .build();
   }

   /**
    * Identifier for this document in the context of the message. By default, message document id is set to
    * document file name. Message document id is normalized (URL encoded).
    *
    * @return document identifier in message
    */
   @Getter
   public String messageDocumentId() {
      return messageDocumentId;
   }

   /**
    * Message document.
    *
    * @return message document
    */
   @Getter
   public Document document() {
      return document;
   }

   @Override
   public boolean equals(Object o) {
      return valueEquals(o);
   }

   @Override
   public int hashCode() {
      return valueHashCode();
   }

   @Override
   public String toString() {
      return valueToString();
   }

   /**
    * Generates a default message document id from specified document. Message document id must be normalized.
    *
    * @param document document
    *
    * @return message document id
    */
   private String defaultMessageDocumentId(Document document) {
      return normalizeMessageDocumentId(document.metadata().documentFileName());
   }

   private String normalizeMessageDocumentId(String messageDocumentId) {
      return URLEncoder.encode(messageDocumentId, StandardCharsets.UTF_8);
   }

   @Override
   public Fields<MessageDocument> domainFields() {
      return domainFields.get();
   }

   public static MessageDocumentBuilder reconstituteBuilder() {
      return new MessageDocumentBuilder().reconstitute(true);
   }

   public static class MessageDocumentBuilder extends DomainBuilder<MessageDocument, MessageDocumentBuilder> {
      private String messageDocumentId;
      private Document document;

      @Setter
      public MessageDocumentBuilder messageDocumentId(String messageDocumentId) {
         this.messageDocumentId = messageDocumentId;

         return this;
      }

      @Setter
      public MessageDocumentBuilder document(Document document) {
         this.document = document;

         return this;
      }

      @Override
      public MessageDocument build() {
         return new MessageDocument(this);
      }
   }

}
