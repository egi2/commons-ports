/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ports.message.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.util.MimeType;

import com.tinubu.commons.ddd.invariant.InvariantValidationException;
import com.tinubu.commons.lang.datetime.ApplicationClock;
import com.tinubu.commons.ports.document.domain.Document.DocumentBuilder;
import com.tinubu.commons.ports.document.domain.DocumentId;
import com.tinubu.commons.ports.message.domain.Message.MessageBuilder;
import com.tinubu.commons.ports.message.domain.MessageContent.MessageContentBuilder;
import com.tinubu.commons.ports.message.domain.MessageDocument.MessageDocumentBuilder;

public class MessageTest {

   private static final ZonedDateTime FIXED_DATE =
         ZonedDateTime.of(LocalDateTime.of(2021, 11, 10, 14, 0, 0), ZoneId.of("UTC"));

   @BeforeAll
   public static void setApplicationClock() {
      ApplicationClock.setFixedClock(FIXED_DATE);
   }

   @Test
   public void testMessageInstanceWhenNominal() {
      assertThat(new MessageBuilder()
                       .from(new MessageAddress("noreply@domain.tld"))
                       .to(new MessageAddress("test@domain.tld"))
                       .subject("subject")
                       .content(MessageContentBuilder.create("content", MimeType.valueOf("text/plain")))
                       .addAttachments(new MessageDocumentBuilder().document(new DocumentBuilder()
                                                             .documentId(new DocumentId("document.txt"))
                                                             .loadedContent("document content")
                                                             .build())
                                             .build())).isNotNull();
   }

   @Test
   public void testMessageInstanceWhenDuplicatedMessageDocumentIds() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> new MessageBuilder()
                  .from(new MessageAddress("noreply@domain.tld"))
                  .to(new MessageAddress("test@domain.tld"))
                  .subject("subject")
                  .content(MessageContentBuilder.create("content", MimeType.valueOf("text/plain")))
                  .addAttachments(new MessageDocumentBuilder()
                                        .document(new DocumentBuilder()
                                                        .documentId(new DocumentId("document.txt"))
                                                        .loadedContent("document content")
                                                        .build())
                                        .build())
                  .addInlines(new MessageDocumentBuilder()
                                    .document(new DocumentBuilder()
                                                    .documentId(new DocumentId("document.txt"))
                                                    .loadedContent("document content")
                                                    .build())
                                    .build())
                  .addAttachments(new MessageDocumentBuilder()
                                        .document(new DocumentBuilder()
                                                        .documentId(new DocumentId("document2.txt"))
                                                        .loadedContent("document content")
                                                        .build())
                                        .build())
                  .addInlines(new MessageDocumentBuilder()
                                    .document(new DocumentBuilder()
                                                    .documentId(new DocumentId("document2.txt"))
                                                    .loadedContent("document content")
                                                    .build())
                                    .build())
                  .build())
            .withMessage("Invariant validation error in "
                         + "[Message[from=MessageAddress[address=noreply@domain.tld],to=MessageAddress[address=test@domain.tld],cc=[],bcc=[],subject=subject,attachments=[document.txt->DocumentId[value=document.txt,relativePath=true,newObject=false],document2.txt->DocumentId[value=document2.txt,relativePath=true,newObject=false]],inlines=[document.txt->DocumentId[value=document.txt,relativePath=true,newObject=false],document2.txt->DocumentId[value=document2.txt,relativePath=true,newObject=false]]]] context : "
                         + "'[MessageDocument[document=Document[documentId=DocumentId[value=document.txt,relativePath=true,newObject=false],metadata=DocumentMetadata[documentName=document.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document.txt], MessageDocument[document=Document[documentId=DocumentId[value=document2.txt,relativePath=true,newObject=false],metadata=DocumentMetadata[documentName=document2.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document2.txt], MessageDocument[document=Document[documentId=DocumentId[value=document.txt,relativePath=true,newObject=false],metadata=DocumentMetadata[documentName=document.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document.txt], MessageDocument[document=Document[documentId=DocumentId[value=document2.txt,relativePath=true,newObject=false],metadata=DocumentMetadata[documentName=document2.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document2.txt]]' has duplicates : "
                         + "{document.txt->["
                         + "MessageDocument[document=Document[documentId=DocumentId[value=document.txt,relativePath=true,newObject=false],metadata=DocumentMetadata[documentName=document.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document.txt],"
                         + "MessageDocument[document=Document[documentId=DocumentId[value=document.txt,relativePath=true,newObject=false],metadata=DocumentMetadata[documentName=document.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document.txt]],"
                         + "document2.txt->["
                         + "MessageDocument[document=Document[documentId=DocumentId[value=document2.txt,relativePath=true,newObject=false],metadata=DocumentMetadata[documentName=document2.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document2.txt],"
                         + "MessageDocument[document=Document[documentId=DocumentId[value=document2.txt,relativePath=true,newObject=false],metadata=DocumentMetadata[documentName=document2.txt,contentType=text/plain;charset=UTF-8,contentSize=16,creationDate=2021-11-10T14:00:00Z,lastUpdateDate=2021-11-10T14:00:00Z,attributes={}],content=LoadedDocumentContent[content=<hidden-value>,contentEncoding=UTF-8]],messageDocumentId=document2.txt]]}");
   }

}